package com.ethiodaily.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ethiodaily.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class SourcesAdapter extends RecyclerView.Adapter<SourcesAdapter.CustomViewHolder> {
        Context context;
        List<HashMap<String, String>> sources;
        private OnItemClickListener listener;

        public interface OnItemClickListener{
                void OnItemClick(int position);
        }
        public void setOnItemClickListenter(OnItemClickListener listener){
                this.listener = listener;
        }

        public SourcesAdapter(Context context, List<HashMap<String, String>> sources) {
                this.context = context;
                this.sources = sources;
        }

        @NonNull
        @Override
        public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(context).inflate(R.layout.source_item,viewGroup,false);
                CustomViewHolder customViewHolder = new CustomViewHolder(view, listener);
                return customViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
                holder.sourceTitle.setText(sources.get(position).get("NAME"));
                holder.sourceWebsite.setText(sources.get(position).get("WEBSITE"));
                InputStream inputStream = null;
                try {
                        inputStream = context.getAssets().open(sources.get(position).get("LOGO")+".png");
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                        holder.sourceLogo.setImageBitmap(bitmap);

                } catch (IOException e) {
                        e.printStackTrace();
                }finally {
                        if (inputStream!= null){
                                try {
                                        inputStream.close();
                                } catch (IOException e) {
                                        e.printStackTrace();
                                }

                        }
                }

        }

        @Override
        public int getItemCount() {
                return sources.size();
        }
        
        public class CustomViewHolder extends RecyclerView.ViewHolder{
                public TextView sourceTitle;
                public TextView sourceWebsite;
                public ImageView sourceLogo;
                public CustomViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
                        super(itemView);
                        sourceTitle = (TextView) itemView.findViewById(R.id.source_title);
                        sourceWebsite = (TextView) itemView.findViewById(R.id.source_website);
                        sourceLogo = (ImageView) itemView.findViewById(R.id.source_logo);
                        itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                        if (listener != null){
                                                if (getAdapterPosition()!= RecyclerView.NO_POSITION){
                                                        listener.OnItemClick(getAdapterPosition());
                                                }
                                        }
                                }
                        });

                }
                
                
        }
}
