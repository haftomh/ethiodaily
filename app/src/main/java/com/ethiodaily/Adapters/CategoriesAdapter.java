package com.ethiodaily.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ethiodaily.Model.Category;
import com.ethiodaily.R;

import java.util.ArrayList;
import java.util.Collections;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CustomCategoriesHolder> implements ItemMoveCallBack.ItemTouchHelperContract {

    ArrayList<Category> categoriesList;
    Context context;

    categoryClickListener clickListener;

    public void setOnCategoryClickListener(categoryClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface categoryClickListener{
        void onRemoveCategory(int position);
    }

    public CategoriesAdapter(ArrayList<Category> categoriesList, Context context) {
        this.categoriesList = categoriesList;
        this.context = context;
    }

    @NonNull
    @Override
    public CustomCategoriesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new CustomCategoriesHolder(LayoutInflater.from(context).inflate(R.layout.category_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CustomCategoriesHolder holder, int position) {
        holder.onBind(position);
        holder.name.setText(categoriesList.get(position).getName());
    }


    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(categoriesList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(categoriesList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onRowSelected(CustomCategoriesHolder myViewHolder) {
        myViewHolder.name.setBackgroundColor(Color.GRAY);

    }

    @Override
    public void onRowClear(CustomCategoriesHolder myViewHolder) {
        myViewHolder.name.setBackgroundColor(Color.WHITE);

    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }



    public class CustomCategoriesHolder extends RecyclerView.ViewHolder{
        TextView name;
        ImageView remove;
        public CustomCategoriesHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            remove = (ImageView) itemView.findViewById(R.id.remove_category);
        }
        public void onBind(final int position){
            name.setText(categoriesList.get(position).getName());
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onRemoveCategory(position);
                }
            });
        }

    }
}
