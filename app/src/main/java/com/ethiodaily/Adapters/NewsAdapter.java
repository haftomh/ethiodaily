package com.ethiodaily.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.agrawalsuneet.dotsloader.loaders.TashieLoader;
import com.bumptech.glide.Glide;
import com.ethiodaily.App.Config;
import com.ethiodaily.Api.News;
import com.ethiodaily.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.BaseViewHolder> {
        Context context;
        ArrayList<News> news;


        boolean favs = false;

        private OnNewsClickListener newsClickListener;

        private static final int VIEW_TYPE_LOADING = 0;
        private static final int VIEW_TYPE_NORMAL = 1;
        private boolean isLoaderVisible = false;

    public NewsAdapter(Context context, ArrayList<News> news) {
                this.context = context;
                this.news = news;
//        Toast.makeText(context, "Datasaver : "+getDatasaverStatus(), Toast.LENGTH_SHORT).show();
        }

    public NewsAdapter(Context context, ArrayList<News> news, boolean favs) {
        this.context = context;
        this.news = news;
        this.favs = favs;
    }

    public interface OnNewsClickListener{
                void OnNewsClick(int position);
                void OnNewsFavorite(int position, boolean fav);
                void OnNewsRemove(int position);
                void onNewsItemClick(int position);

        }

        public void setOnNewsClickListenter(OnNewsClickListener listener){
                this.newsClickListener = listener;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
/*
                View view = LayoutInflater.from(context).inflate(R.layout.news_item,viewGroup,false);
                CustomViewHolder customViewHolder = new CustomViewHolder(view, newsClickListener);
                return customViewHolder;
*/
            View view = null;
            CustomViewHolder customViewHolder;
            switch (i) {
                case VIEW_TYPE_NORMAL:
                    view = LayoutInflater.from(context).inflate(R.layout.news_item_for_u, viewGroup, false);
                    if (news.size()>0){
                        if (news.get(0).getCategorie().equals("Breaking")){
                            view = LayoutInflater.from(context).inflate(R.layout.news_item, viewGroup, false);
                        }
                    }

                    customViewHolder = new CustomViewHolder(view, newsClickListener);
                    return customViewHolder;

                case VIEW_TYPE_LOADING:
                    View loading;

                    TashieLoader tashie = new TashieLoader(
                            context, 5,
                            8, 10,
                            ContextCompat.getColor(context, R.color.green));

                    tashie.setAnimDuration(80);
                    tashie.setAnimDelay(200);
                    tashie.setInterpolator(new AccelerateInterpolator());


                    view = LayoutInflater.from(context).inflate(R.layout.item_progress, viewGroup, false);
//                    ((LinearLayout)view).addView(tashie);
                    ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
                    return progressViewHolder;
                default:
                    return null;
            }
        }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position, @NonNull List<Object> payloads) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
//        Toast.makeText(context, " "+isLoaderVisible, Toast.LENGTH_SHORT).show();
        if (isLoaderVisible) {
            return position == news.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
        public void onBindViewHolder(@NonNull final BaseViewHolder holder, final int position) {
            holder.onBind(position);

        }


        @Override
        public int getItemCount() {
                return news.size();
        }

    public void addItems(List<News> newsItems) {
        news.addAll(newsItems);

//        notifyItemInserted(newsItems.size()-newsItems.size()-2);
        notifyDataSetChanged();
    }

    public void addLoading() {
        isLoaderVisible = true;
//        news.add( new News());
//        notifyItemInserted(news.size() - 1);
    }

    public void removeLoading() {
        isLoaderVisible = false;
/*
        int position = news.size()-1;
        News item = getItem(position);
        if (item != null) {
            news.remove(position);
            notifyItemRemoved(position);
        }
*/
    }

    private News getItem(int position) {
        return news.get(position);
    }

    public class CustomViewHolder extends BaseViewHolder {
                public TextView newsTitleTV;
                public TextView newsSourceTV;
                public ImageView removeNews;
                public ImageView favoriteNews;
                public ImageView newsSourceLogo, newsImage;


        public ImageView videoIndicator;

                public CustomViewHolder(@NonNull View itemView, final OnNewsClickListener listener) {
                        super(itemView);
                        newsTitleTV = (TextView) itemView.findViewById(R.id.news_title);
                        newsSourceTV = (TextView) itemView.findViewById(R.id.source_news_name);
                        removeNews = (ImageView) itemView.findViewById(R.id.remove_news);
                        favoriteNews = (ImageView) itemView.findViewById(R.id.favorite_news);
                        newsSourceLogo = (ImageView) itemView.findViewById(R.id.source_news_logo);
                        newsImage = (ImageView) itemView.findViewById(R.id.news_image);
                        videoIndicator = (ImageView) itemView.findViewById(R.id.video_indicator);


                        itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                        if (listener != null){
                                                if (getAdapterPosition()!= RecyclerView.NO_POSITION){
                                                    if (news.get(getAdapterPosition()).getType().equals("video")){
                                                        listener.onNewsItemClick(getAdapterPosition());
                                                    }
                                                    else {
                                                            System.out.println("starting "+getAdapterPosition());
                                                            Toast.makeText(context, getCurrentPosition()+"  "+getAdapterPosition(), Toast.LENGTH_SHORT).show();
                                                        listener.OnNewsClick(getAdapterPosition());
                                                    }
                                                }
                                        }
                                }
                        });
                        removeNews.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                        if (newsClickListener != null){
                                                if (getLayoutPosition()!= RecyclerView.NO_POSITION){
                                                        newsClickListener.OnNewsRemove(getAdapterPosition());
                                                }
                                        }

                                }
                        });
                        favoriteNews.setOnClickListener(new View.OnClickListener() {
                                @SuppressLint("ResourceAsColor")
                                @Override
                                public void onClick(View view) {
                                        if (newsClickListener != null){
                                                if (getAdapterPosition()!= RecyclerView.NO_POSITION){
/*
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                                favoriteNews.setImageDrawable(context.getDrawable(R.drawable.favorite_selected));
                                                        }
                                                        else {
*/
//                                                        }
                                                        boolean fav = news.get(getAdapterPosition()).isFav();
                                                        if (fav){
                                                            fav = false;
                                                            favoriteNews.setImageResource(R.drawable.ic_fav);
                                                        }else {
                                                            fav = true;
                                                            favoriteNews.setImageResource(R.drawable.favorite_selected);
                                                        }

                                                        newsClickListener.OnNewsFavorite(getAdapterPosition(), fav);
                                                }
                                        }

                                }
                        });
                }


        @Override
        public void onBind(int position) {
//                Toast.makeText(context, "Position : "+position, Toast.LENGTH_SHORT).show();
            String newsTitle = news.get(position).getTitle();
            newsTitleTV.setText(newsTitle);
            newsSourceTV.setText(news.get(position).getSource());

//            if (!getDatasaverStatus()){
                Glide.with(context).load(Config.base +"" +
                        "storage/cover_image/"+news.get(position).getCover_image())
                        .fitCenter()
                        .onlyRetrieveFromCache(getDatasaverStatus())
                        .placeholder(R.drawable.ic_image)
                        .into(newsImage);
/*
            }
            else {
                Glide.with(context).load(news.get(position).getImage_url())
                        .fitCenter()
                        .onlyRetrieveFromCache(true)
                        .placeholder(R.drawable.ic_image)
                        .into(newsImage);

            }
*/


            if (favs){
                removeNews.setVisibility(View.GONE);
                favoriteNews.setVisibility(View.VISIBLE);
            }


            boolean fav = news.get(position).isFav();
            if (fav){
                favoriteNews.setImageResource(R.drawable.favorite_selected);
            }else {
                favoriteNews.setImageResource(R.drawable.ic_fav);
            }


            if (news.get(position).getType().equals("video")){
                videoIndicator.setVisibility(View.VISIBLE);
//                videoIndicator.setImageResource(R.drawable.ic_video);
            }


            InputStream inputStream = null;
            try {
                inputStream = context.getAssets().open(news.get(position).getSource() + ".png");
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                newsSourceLogo.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        public void setNewsImage(int position, Uri uri){
             newsSourceLogo.setImageURI(uri);
        }


        @Override
        protected void clear() {

        }
    }

    private boolean getDatasaverStatus() {

        /*getting data saver mode status from sharedpreferences*/

        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        boolean datasaver = preferences.getBoolean("datasaver", true);
//        Toast.makeText(context, "Datasaver : "+datasaver, Toast.LENGTH_SHORT).show();

        return datasaver;
    }

    public class ProgressViewHolder extends BaseViewHolder{
        public ProgressViewHolder(@NonNull View itemView) {
            super(itemView);
//            TashieLoader tashieLoader = itemView.findViewById(R.id.tashie_loader);
//            tashieLoader.initView();
        }

        @Override
        protected void clear() {

        }


    }

/*
    private class LoadImage extends AsyncTask<List<NewsImage>, Integer, Long> {

        @Override
        protected Long doInBackground(List<NewsImage>... lists) {
            List<NewsImage> NewsImageList = lists[0];
            for(NewsImage newsImage : NewsImageList){
                ImageView imageView = newsImage.getImageView();
                Uri uri = Uri.parse("https://fanabc.com/wp-content/uploads/2019/10/aby.jpg");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    }
*/

    public class NewsImage{
        ImageView imageView;
        String url;
        int position;

        public NewsImage(ImageView imageView, String url, int position) {
            this.imageView = imageView;
            this.url = url;
            this.position = position;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public void setImageView(ImageView imageView) {
            this.imageView = imageView;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
        private int mCurrentPosition;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }

        protected abstract void clear();

        public void onBind(int position) {
            mCurrentPosition = position;
            clear();
        }

        public int getCurrentPosition() {
            return mCurrentPosition;
        }

    }

}
