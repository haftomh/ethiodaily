package com.ethiodaily.Adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ethiodaily.Api.News;
import com.ethiodaily.App.Config;
import com.ethiodaily.R;
import com.ethiodaily.Util.Utils;

public class NewsPagedAdapter extends PagedListAdapter<com.ethiodaily.Api.News, NewsPagedAdapter.BaseViewHolder> {

    News news;

    private static final int VIEW_TYPE_LOADING = 1;
    private static final int VIEW_TYPE_NORMAL = 0;
    private boolean isLoaderVisible = false;

    boolean favs = false;
    private NewsPagedAdapter.OnNewsClickListener newsClickListener;
    private Context context;


    public NewsPagedAdapter() {
        super(NEWS_COMPARATOR);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public interface OnNewsClickListener{
        void OnNewsClick(News news);
        void OnNewsFavorite(int position, boolean fav);
        void OnNewsRemove(int position);
        void onNewsItemClick(News news);

    }

    public void setOnNewsClickListenter(NewsPagedAdapter.OnNewsClickListener listener){
        this.newsClickListener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        NewsPagedAdapter.CustomViewHolder customViewHolder;

        switch (viewType) {
            case VIEW_TYPE_LOADING:
                View loading;

                loading = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
//                    ((LinearLayout)view).addView(tashie);
                NewsPagedAdapter.ProgressViewHolder progressViewHolder = new NewsPagedAdapter.ProgressViewHolder(loading);
                return progressViewHolder;
            case VIEW_TYPE_NORMAL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item_for_u, parent, false);
//                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item, parent, false);

                customViewHolder = new NewsPagedAdapter.CustomViewHolder(view, newsClickListener);
                return customViewHolder;

            default:
                View load = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
                return new NewsPagedAdapter.ProgressViewHolder(load);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        System.out.println("Hello");
        holder.bind(getItem(position));
//        holder.onBind(position);
    }

    public class CustomViewHolder extends NewsPagedAdapter.BaseViewHolder {
        public TextView newsTitleTV;
        public TextView newsSourceTV;
        public TextView timeTV;
        public ImageView removeNews;
        public ImageView favoriteNews;
        public ImageView newsSourceLogo, newsImage;


        public ImageView videoIndicator;
        public OnNewsClickListener listener;

        public CustomViewHolder(@NonNull View itemView, final NewsPagedAdapter.OnNewsClickListener listener) {
            super(itemView);
            this.listener = listener;
            newsTitleTV = (TextView) itemView.findViewById(R.id.news_title);
            newsSourceTV = (TextView) itemView.findViewById(R.id.source_news_name);
            timeTV = (TextView) itemView.findViewById(R.id.time);
            newsImage = (ImageView) itemView.findViewById(R.id.news_image);
            videoIndicator = (ImageView) itemView.findViewById(R.id.video_indicator);
            favoriteNews = (ImageView) itemView.findViewById(R.id.favorite_news);
            removeNews = (ImageView) itemView.findViewById(R.id.remove_news);
            newsSourceLogo = (ImageView) itemView.findViewById(R.id.source_news_logo);




            removeNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (newsClickListener != null){
                        if (getAdapterPosition()!= RecyclerView.NO_POSITION){
                            newsClickListener.OnNewsRemove(getAdapterPosition());
                        }
                    }

                }
            });
            favoriteNews.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View view) {
                    if (newsClickListener != null){
                        if (getAdapterPosition()!= RecyclerView.NO_POSITION){
/*
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                                favoriteNews.setImageDrawable(context.getDrawable(R.drawable.favorite_selected));
                                                        }
                                                        else {
*/
//                                                        }
//                            boolean fav = news.isFav();
                            boolean fav = false;
                            if (fav){
                                fav = false;
                                favoriteNews.setImageResource(R.drawable.ic_fav);
                            }else {
                                fav = true;
                                favoriteNews.setImageResource(R.drawable.favorite_selected);
                            }

                            newsClickListener.OnNewsFavorite(getAdapterPosition(), fav);
                        }
                    }

                }
            });
//            onBind(0);
            
        }


        @Override
        public void onBind(int position) {
//                Toast.makeText(context, "Position : "+position, Toast.LENGTH_SHORT).show();
            String newsTitle = news.getTitle();
            newsTitleTV.setText(newsTitle);
            newsSourceTV.setText(news.getSource());

//            if (!getDatasaverStatus()){
            Glide.with(context).load(Config.baseUrl +"" +
                    "storage/cover_image/"+news.getCover_image())
                    .fitCenter()
                    .onlyRetrieveFromCache(getDatasaverStatus())
                    .placeholder(R.drawable.ic_image)
                    .into(newsImage);


            if (favs){
                removeNews.setVisibility(View.GONE);
                favoriteNews.setVisibility(View.VISIBLE);
            }


//            boolean fav = news.isFav();
            boolean fav = false;
            if (fav){
                favoriteNews.setImageResource(R.drawable.favorite_selected);
            }else {
                favoriteNews.setImageResource(R.drawable.ic_fav);
            }


            if (news.getType().equals("video")){
                videoIndicator.setVisibility(View.VISIBLE);
//                videoIndicator.setImageResource(R.drawable.ic_video);
            }



        }



        @Override
        protected void clear() {

        }

        @Override
        protected void bind(Object item) {
            final News newsItem = (News) item;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        if (getAdapterPosition()!= RecyclerView.NO_POSITION){
                            if (newsItem.getType().toLowerCase().equals("video")){
                                listener.onNewsItemClick(newsItem);
                            }
                            else {
                                listener.OnNewsClick(newsItem);
                            }
                        }
                    }
                }
            });
            System.out.println("Object Item : "+item.toString());
            String newsTitle = newsItem.getTitle();
            System.out.println("Title : "+ newsItem);
            newsTitleTV.setText(newsTitle);
            newsSourceTV.setText(newsItem.getSource());

            timeTV.setText(Utils.timeDifference(newsItem.getCreated_at()));

            System.out.println("Image : "+newsItem.getCover_image());
//            if (!getDatasaverStatus()){
            Glide.with(getContext()).load(Config.baseUrl+
                    "storage/cover_image/"+newsItem.getCover_image())
                    .fitCenter()
                    .onlyRetrieveFromCache(getDatasaverStatus())
                    .placeholder(R.drawable.ic_image_black_48dp)
                    .into(newsImage);
                if (newsItem.getType().toLowerCase().equals("video")){
                    videoIndicator.setVisibility(View.VISIBLE);
//                videoIndicator.setImageResource(R.drawable.ic_video);
                }
                else {
                    videoIndicator.setVisibility(View.GONE);
                }



        }
    }

    private static final DiffUtil.ItemCallback<com.ethiodaily.Api.News> NEWS_COMPARATOR = new DiffUtil.ItemCallback<com.ethiodaily.Api.News>() {
        @Override public boolean areItemsTheSame(@NonNull com.ethiodaily.Api.News oldItem, @NonNull com.ethiodaily.Api.News newItem) {
            return oldItem.getId() == newItem.getId();
        }
        @SuppressLint("DiffUtilEquals")
        @Override public boolean areContentsTheSame(@NonNull com.ethiodaily.Api.News oldItem, @NonNull com.ethiodaily.Api.News newItem) {
            return oldItem == newItem;
        }
    };
    private boolean getDatasaverStatus() {

        /*getting data saver mode status from sharedpreferences*/

        SharedPreferences preferences = getContext().getApplicationContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        boolean datasaver = preferences.getBoolean("datasaver", false);
//        Toast.makeText(context, "Datasaver : "+datasaver, Toast.LENGTH_SHORT).show();

        return datasaver;
    }

    public class ProgressViewHolder extends NewsPagedAdapter.BaseViewHolder {
        public ProgressViewHolder(@NonNull View itemView) {
            super(itemView);
//            TashieLoader tashieLoader = itemView.findViewById(R.id.tashie_loader);
//            tashieLoader.initView();
        }

        @Override
        protected void clear() {

        }

        @Override
        protected void bind(Object item) {

        }


    }

    public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
        private int mCurrentPosition;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }

        protected abstract void clear();
        protected abstract void bind(Object item);

        public void onBind(int position) {
            mCurrentPosition = position;
            clear();
        }

        public int getCurrentPosition() {
            return mCurrentPosition;
        }

/*        public void bind(News item){
            System.out.println("News : "+item.getTitle());
            news = item;

        }*/
    }
}
