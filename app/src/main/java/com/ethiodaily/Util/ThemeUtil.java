package com.ethiodaily.Util;


import androidx.appcompat.app.AppCompatActivity;

import com.ethiodaily.R;

/**
 * Created by Pankaj on 12-11-2017.
 */

public class ThemeUtil extends AppCompatActivity {
    public static final int THEME_RED = 0;
    public static final int THEME_PINK = 1;
    public static final int THEME_PURPLE = 2;
    public static final int THEME_DEEPPURPLE = 3;
    public static final int THEME_INDIGO = 4;
    public static final int THEME_BLUE = 5;
    public static final int THEME_LIGHTBLUE = 6;
    public static final int THEME_CYAN = 7;
    public static final int THEME_TEAL = 8;
    public static final int THEME_GREEN = 9;
    public static final int THEME_LIGHTGREEN = 10;
    public static final int THEME_LIME = 11;
    public static final int THEME_YELLOW = 12;
    public static final int THEME_AMBER = 13;
    public static final int THEME_ORANGE = 14;
    public static final int THEME_DEEPORANGE = 15;
    public static final int THEME_BROWN = 16;
    public static final int THEME_GRAY = 17;
    public static final int THEME_BLUEGRAY = 18;

    public static int getThemeId(int theme){
        int themeId=0;
        switch (theme){
            case THEME_RED  :
                themeId = R.style.AppTheme;
                break;
            case THEME_PINK  :
                themeId = R.style.DarkTheme;
                break;
            case THEME_PURPLE  :
                themeId = R.style.DarkTheme;
                break;
            default:
                break;
        }
        return themeId;
    }

    public void changeTheme(){
        setTheme(R.style.DarkTheme);
    }
    
}
