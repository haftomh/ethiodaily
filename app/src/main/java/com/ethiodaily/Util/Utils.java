package com.ethiodaily.Util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ethiodaily.EthioVoice;

import java.sql.Timestamp;
import java.util.Date;

public class Utils {
    public static void shareText(Context context, String text){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT,text);
        shareIntent.setType("TEXT/PLAIN");
        context.startActivity(Intent.createChooser(shareIntent, "Share via..."));
    }
    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) EthioVoice.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }
    public static String timeDifference(String date){
        java.util.Date now = new java.util.Date();

        Timestamp timestampNow = new Timestamp(now.getTime());

        // create a calendar and assign it the same time

        // create a  second time stamp
        Timestamp timestamp2 = Timestamp.valueOf(date);
        Date date1 = new Date(timestamp2.getTime());
        System.out.println("Date : "+timestamp2.getTime());
        System.out.println("Today : "+timestampNow.getTime());
        // get time difference in seconds
        double milliseconds = timestampNow.getTime() - timestamp2.getTime();
        double seconds =  milliseconds / 1000;
        System.out.println("Time difference in Seconds "+seconds);

        int years = (int) seconds/31557600;
        int months = (int) (seconds)/2592000;
        int weeks = (int) (seconds)/604800;
        int days = (int) (seconds)/86400;
        // calculate hours minutes and seconds
        int hours = (int) seconds / 3600;

        System.out.println("years "+years);
        System.out.println("months "+months);
        System.out.println("weeks "+weeks);
        System.out.println("days "+days);
        System.out.println("hours "+hours);

        if(years>0){
            return years + " yrs";
        }
        else if(months>0){
            return months + "m";
        }
        else if(weeks > 0){
            return weeks + " w";
        }
        else if(days > 0){
            return days + " d";
        }
        else if(hours > 0){
            return hours+" hr";
        }
        else {
            return "now";
        }
    }
}
