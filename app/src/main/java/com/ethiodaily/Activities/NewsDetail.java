package com.ethiodaily.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.ethiodaily.Api.News;
import com.ethiodaily.App.Config;
import com.ethiodaily.DB.DBHandler;
import com.ethiodaily.R;
import com.ethiodaily.Util.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class NewsDetail extends AppCompatActivity implements View.OnClickListener {

    private WebView webView;
    private TextView title,source,date;
    private FloatingActionButton favourite;
    private News news;
    private ImageButton favorite, share, like, dislike;
    private ImageView detailImage;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        news = (News) getIntent().getSerializableExtra("NEWS");
        setContentView(R.layout.activity_news_detail_1);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
//        Toast.makeText(this, "Connection:  "+ Utils.isConnected(), Toast.LENGTH_SHORT).show();


        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

/*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
*/


        mAdView = findViewById(R.id.adView);
        final AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                NewsDetail.this.runOnUiThread(new Runnable() {
                    public void run() {
                        mAdView.loadAd(adRequest);

                    }
                });
            }

        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 1000 * 10);


        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });


        initReactionImageButtons();

        title = (TextView) findViewById(R.id.newstitle);
        source = (TextView) findViewById(R.id.source_name);
        date = (TextView) findViewById(R.id.date);
        detailImage = (ImageView) findViewById(R.id.news_detail_imageview);
        favourite = (FloatingActionButton) findViewById(R.id.favourite);
        favourite.setOnClickListener(this);
        boolean isFav = isFavorite(news);
        if (isFav){
            favorite.setBackgroundColor(Color.WHITE);
            favorite.setImageResource(R.drawable.favorite_selected_48);
            Toast.makeText(this, "Fav", Toast.LENGTH_SHORT).show();
        }
        else {
            favourite.setImageResource(R.drawable.ic_fav);
            favorite.setImageResource(R.drawable.ic_fav);
            Toast.makeText(this, "Not Fav", Toast.LENGTH_SHORT).show();
        }
//        TextView title_details = (TextView) findViewById(R.id.title_detail_news);
//        title_details.setText("Hello World wow amazing");
        webView = (WebView) findViewById(R.id.webview);
//        initCollapsingToolbar();
        System.out.println("Detail : "+news.getComment());
        String newsHtml = "<html><body>" +
                 news.getComment()+
                "</body></html>";
        String customHtml = "<html><body>" +
                "<p>" +
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet consectetur adipiscing elit pellentesque habitant. Adipiscing commodo elit at imperdiet dui accumsan sit amet nulla. Euismod lacinia at quis risus sed vulputate odio ut. Aenean et tortor at risus viverra adipiscing at. Cras pulvinar mattis nunc sed blandit libero volutpat sed. Consequat mauris nunc congue nisi vitae. Mollis nunc sed id semper risus in hendrerit gravida rutrum. Aliquet bibendum enim facilisis gravida neque. Lorem mollis aliquam ut porttitor leo a. Nam aliquam sem et tortor. Odio euismod lacinia at quis risus sed vulputate odio ut. Integer vitae justo eget magna. Iaculis urna id volutpat lacus. Elementum eu facilisis sed odio morbi quis. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh. Eget sit amet tellus cras adipiscing enim eu turpis egestas. Eu ultrices vitae auctor eu augue ut lectus arcu. Velit sed ullamcorper morbi tincidunt ornare. Pharetra convallis posuere morbi leo urna molestie at elementum eu.\n" +
                "</p>" +
                "<p>"+
                "In nisl nisi scelerisque eu ultrices vitae. Suscipit tellus mauris a diam maecenas sed enim. Velit ut tortor pretium viverra suspendisse. Sem viverra aliquet eget sit amet tellus cras. Sagittis orci a scelerisque purus semper eget. Mauris vitae ultricies leo integer malesuada nunc. Eu non diam phasellus vestibulum lorem sed risus. Blandit massa enim nec dui. Consequat id porta nibh venenatis cras sed. Praesent tristique magna sit amet purus gravida quis. Amet risus nullam eget felis eget nunc lobortis. Amet mauris commodo quis imperdiet. Consectetur lorem donec massa sapien faucibus. Facilisis gravida neque convallis a cras semper auctor neque vitae. Tempus egestas sed sed risus pretium quam vulputate dignissim. Elementum sagittis vitae et leo duis ut diam quam nulla. Morbi blandit cursus risus at ultrices. Sagittis purus sit amet volutpat consequat mauris nunc. Sit amet dictum sit amet justo donec enim diam. Id cursus metus aliquam eleifend mi.\n" +
                "</p>"+
                "<p>"+
                "Enim blandit volutpat maecenas volutpat blandit aliquam. Fermentum posuere urna nec tincidunt praesent semper feugiat nibh sed. Ornare quam viverra orci sagittis. Egestas congue quisque egestas diam in arcu cursus euismod. Donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Tempus egestas sed sed risus pretium. Urna id volutpat lacus laoreet non curabitur gravida arcu ac. Mauris cursus mattis molestie a iaculis at. Tortor at risus viverra adipiscing at in. Vitae aliquet nec ullamcorper sit amet risus nullam eget. Ullamcorper a lacus vestibulum sed. Neque sodales ut etiam sit amet nisl purus. Morbi non arcu risus quis varius quam quisque id diam. Lorem sed risus ultricies tristique nulla aliquet. Mauris ultrices eros in cursus turpis massa tincidunt dui ut. Enim blandit volutpat maecenas volutpat blandit aliquam etiam. Dui ut ornare lectus sit. In aliquam sem fringilla ut morbi tincidunt. Feugiat vivamus at augue eget. In eu mi bibendum neque egestas congue quisque egestas.\n" +
                "</p>"+
                "<p>"+
                "Tempus imperdiet nulla malesuada pellentesque elit eget. Massa sapien faucibus et molestie ac. Vulputate ut pharetra sit amet aliquam. Ut porttitor leo a diam sollicitudin tempor id eu. Etiam non quam lacus suspendisse faucibus interdum posuere. Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi tristique. Netus et malesuada fames ac turpis egestas integer eget aliquet. Ornare lectus sit amet est placerat in egestas erat. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus et. Quis blandit turpis cursus in. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Felis eget velit aliquet sagittis id consectetur purus ut. Mi quis hendrerit dolor magna eget est lorem ipsum. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit. Gravida quis blandit turpis cursus in hac habitasse platea dictumst. Dictum non consectetur a erat nam at. Eget nunc scelerisque viverra mauris in aliquam. Cum sociis natoque penatibus et magnis dis parturient. Sed lectus vestibulum mattis ullamcorper.\n" +
                "</p>"+
                "<p>"+
                "Rhoncus mattis rhoncus urna neque viverra justo. Ut tortor pretium viverra suspendisse potenti nullam ac tortor. Donec et odio pellentesque diam volutpat. Ultrices neque ornare aenean euismod elementum nisi. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Urna nunc id cursus metus aliquam eleifend mi in. Duis at tellus at urna condimentum mattis pellentesque. Cursus euismod quis viverra nibh cras pulvinar. Commodo sed egestas egestas fringilla. Hendrerit dolor magna eget est lorem. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. In nibh mauris cursus mattis molestie a iaculis at erat. Vulputate odio ut enim blandit volutpat maecenas volutpat blandit.\n" +
                "</p>"+
                "<p>"+
                "Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Non pulvinar neque laoreet suspendisse. Cras pulvinar mattis nunc sed blandit libero. Cursus sit amet dictum sit amet justo. Ultrices mi tempus imperdiet nulla. Vel orci porta non pulvinar neque. Id ornare arcu odio ut sem nulla. Iaculis nunc sed augue lacus viverra vitae congue. Eget sit amet tellus cras adipiscing enim eu turpis egestas. Hendrerit gravida rutrum quisque non tellus orci ac auctor. Sed libero enim sed faucibus turpis in eu." +
                "</p" +
                "</body></html>";
        String videoUrl = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eWEF1Zrmdow\" frameborder=\"0\" allowfullscreen></iframe>";
        title.setText(news.getTitle());
        source.setText(news.getSource());
        date.setText(news.getDate());
        DrawableCrossFadeFactory factory =
                new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();

//        if (!getDatasaverStatus()){
//                    .load("http://192.168.101.33:8000/storage/cover_image/WIN_20191016_11_06_05_Pro.jpg_1571227859.jpg")
            Glide.with(this)
                    .load(Config.base +"" +
                            "storage/cover_image/"+news.getCover_image())
                    .onlyRetrieveFromCache(getDatasaverStatus())
                    .transition(withCrossFade(factory))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_image_black_48dp)
                    .fitCenter()
                    .into(detailImage);

//        }
        webView.getSettings().setJavaScriptEnabled(true);


        webView.setWebChromeClient(new WebChromeClient() {

            }
            );



//        webView.loadUrl("https://fanabc.com/tigrigna/blog/2019/09/21/%e1%88%8d%e1%8b%95%e1%88%8a-68-%e1%88%9a%e1%88%8d%e1%8b%ac%e1%8a%95-%e1%89%a5%e1%88%ad-%e1%8b%88%e1%8d%83%e1%8a%a2-%e1%8b%9d%e1%89%b0%e1%8c%88%e1%89%a0%e1%88%a8%e1%88%89-%e1%88%86%e1%89%b4%e1%88%8d/");
//        webView.loadUrl("https://www.youtube.com/watch?v=6I1ujFtU0Cc");
//        webView.setHorizontalScrollBarEnabled(false);

//        webView.loadData(videoUrl, "text/html" , "utf-8");
        System.out.println("Encoding : "+webView.getSettings().getDefaultTextEncodingName());
        webView.loadData(newsHtml, "text/html; charset=utf-8","utf-16");
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        System.out.println("Encoding 2: "+webView.getSettings().getDefaultTextEncodingName());
    }

    private void initReactionImageButtons() {
        favorite = (ImageButton) findViewById(R.id.favorite);
        share = (ImageButton) findViewById(R.id.share);
        like = (ImageButton) findViewById(R.id.like);
        dislike = (ImageButton) findViewById(R.id.dislike);

        favorite.setOnClickListener(this);
        share.setOnClickListener(this);
        like.setOnClickListener(this);
        dislike.setOnClickListener(this);
    }

    private boolean getDatasaverStatus() {

        /*getting data saver mode status from sharedpreferences*/

        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);
        boolean datasaver = preferences.getBoolean("datasaver", false);
//        Toast.makeText(context, "Datasaver : "+datasaver, Toast.LENGTH_SHORT).show();

        return datasaver;
    }



    /**
 * Initializing collapsing toolbar
 * Will show and hide the toolbar txtPostTitle on scroll
 */


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.favourite:
                if (news.isFav()){
                    DBHandler.favoriteNews(news, false);
                    favourite.setImageResource(R.drawable.ic_fav);
                    news.setFav(false);
                }
                else {
                    DBHandler.favoriteNews(news, true);
                    favourite.setImageResource(R.drawable.favorite_selected_48);
                    news.setFav(true);
                }
                break;
/*            case R.id.share_news_detail:
                Utils.shareText(this, news.getTitle());
                break;*/

            case R.id.share:
                Utils.shareText(this, news.getTitle());
                break;
            case R.id.like:
                like.setImageResource(R.drawable.ic_like_selected);
                dislike.setImageResource(R.drawable.ic_dislike);
//                Toast.makeText(this, "Liked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dislike:

                dislike.setImageResource(R.drawable.ic_dislike_selected);
                like.setImageResource(R.drawable.ic_like);
//                Toast.makeText(this, "disliked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.favorite:
                boolean isFav = isFavorite(news);
                if (isFav){
                    DBHandler.favoriteNews(news, false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        favorite.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorPrimary));
                    }
                    this.favorite.setImageResource(R.drawable.ic_fav);
                    news.setFav(false);
                }
                else {
                    DBHandler.favoriteNews(news, true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        favorite.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorAccent));
                    }
                    this.favorite.setImageResource(R.drawable.favorite_selected_48);
                    news.setFav(true);
                }
                break;
            default:
                break;


        }
    }

    private boolean isFavorite(News news) {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<News> results = realm.where(News.class).equalTo("id",news.getId()).findAll();
        if (results.isEmpty()){
            return false;
        }
        else {
            return true;
        }
    }


}