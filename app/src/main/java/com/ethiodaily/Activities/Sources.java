package com.ethiodaily.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ethiodaily.Adapters.SourcesAdapter;
import com.ethiodaily.Api.DataParser;
import com.ethiodaily.Api.GetDataService;
import com.ethiodaily.Api.News;
import com.ethiodaily.Api.RetrofitClientInstance;
import com.ethiodaily.PaginationScrollListener;
import com.ethiodaily.R;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ethiodaily.PaginationScrollListener.PAGE_START;

public class Sources extends AppCompatActivity {

    private RecyclerView sourcesList;
    LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    //    pagination
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sources);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<News>> call = service.getAllPhotos();
        System.out.println("Response : "+call.toString());
        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                generateDataList(response.body());
                System.out.println("Success :");
                Toast.makeText(Sources.this, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Toast.makeText(Sources.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                Toast.makeText(Sources.this, "Error : "+t.toString(), Toast.LENGTH_SHORT).show();
                System.out.println("ERROR : "+t.toString());
            }
        });


        sourcesList = (RecyclerView) findViewById(R.id.sources_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        DataParser dataParser = new DataParser(this);
        final List<HashMap<String, String>> sources = dataParser.ParseData("sources","");
        final SourcesAdapter sourcesAdapter = new SourcesAdapter(this, sources);
        sourcesAdapter.setOnItemClickListenter(new SourcesAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
/*
                sources.remove(position);
                sourcesAdapter.notifyItemRemoved(position);
*/
            }
        });
        sourcesList.setHasFixedSize(true);
        sourcesList.setLayoutManager(gridLayoutManager);
        sourcesList.setAdapter(sourcesAdapter);

        sourcesList.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        loadNextPage();
                    }
                },1000);
            }

            @Override
            protected int getTotalPageCount() {
                return totalPage;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        loadFirstPage();
    }

    private void generateDataList(List<News> photoList) {
        for (News photo: photoList) {
            System.out.println("Id : "+photo.getId());
            System.out.println("Title : "+photo.getTitle());
            System.out.println("Url : "+photo.getCover_image());
            Toast.makeText(this, "Title : "+photo.getTitle(), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadFirstPage() {

    }
}
