package com.ethiodaily.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.ethiodaily.Adapters.CategoriesAdapter;
import com.ethiodaily.Adapters.ItemMoveCallBack;
import com.ethiodaily.Model.Category;
import com.ethiodaily.R;

import java.util.ArrayList;

public class CategorySelection extends AppCompatActivity {

    RecyclerView categoriesRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_selection);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        categoriesRecyclerView = (RecyclerView) findViewById(R.id.categories_recyclerview);

        ArrayList<String> categoriesList = new ArrayList<>();
        categoriesList.add("laksfd");
        categoriesList.add("laksfd");
        categoriesList.add("laksfd");
        categoriesList.add("laksfd");
        final ArrayList<Category> categoryArrayList = new ArrayList<>();
        Category category = new Category(1,"Breaking", 1, true);

        categoryArrayList.add(new Category(1,"Breaking", 1, true));
        categoryArrayList.add(new Category(2,"Politics", 2, true));
        categoryArrayList.add(new Category(3,"Sport", 3, true));
        categoryArrayList.add(new Category(4,"Business", 4, false));
        categoryArrayList.add(new Category(5,"Entertainment", 5, true));
        categoryArrayList.add(new Category(6,"Psychology", 6, true));

        final CategoriesAdapter categoriesAdapter = new CategoriesAdapter(categoryArrayList, this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);

        ItemTouchHelper.Callback callback = new ItemMoveCallBack(categoriesAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);

        touchHelper.attachToRecyclerView(categoriesRecyclerView);

//        categoriesRecyclerView.setLayoutManager(gridLayoutManager);
        categoriesRecyclerView.setAdapter(categoriesAdapter);


        categoriesAdapter.setOnCategoryClickListener(new CategoriesAdapter.categoryClickListener() {
            @Override
            public void onRemoveCategory(int position) {
                categoryArrayList.remove(position);
                categoriesAdapter.notifyItemRemoved(position);
            }
        });
    }
}
