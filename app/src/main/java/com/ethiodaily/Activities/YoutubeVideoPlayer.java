package com.ethiodaily.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.ethiodaily.Api.News;
import com.ethiodaily.DB.DBHandler;
import com.ethiodaily.R;
import com.ethiodaily.Util.Utils;
import com.ethiodaily.config.YouTubeConfig;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import io.realm.Realm;
import io.realm.RealmResults;

public class YoutubeVideoPlayer extends YouTubeBaseActivity implements View.OnClickListener, YouTubePlayer.OnInitializedListener, YouTubePlayer.OnFullscreenListener {

//    YouTubePlayerView youTubePlayerView;
    YouTubePlayerView youTubePlayerView;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private YouTubePlayer myouTubePlayer;
    News news;
    private ImageButton favorite, share, like, dislike;
/*
    YouTubePlayer.OnInitializedListener onInitializedListener;
    YouTubePlayer.OnFullscreenListener onFullscreenListener;
*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(YouTubeConfig.getApiKey(), this);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final boolean fullScreenVideo = loadFullScreenVideo();
        news = (News) getIntent().getSerializableExtra("NEWS");
        setContentView(R.layout.activity_youtube_video_player);

        boolean isFav = isFavorite(news);
/*
        if (isFav){
            favorite.setBackgroundColor(Color.WHITE);
            favorite.setImageResource(R.drawable.favorite_selected_48);
            Toast.makeText(this, "Fav", Toast.LENGTH_SHORT).show();
        }
        else {
            favorite.setImageResource(R.drawable.ic_fav);
            Toast.makeText(this, "Not Fav", Toast.LENGTH_SHORT).show();
        }

*/

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);

/*
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
*/
/*
                myouTubePlayer = youTubePlayer;
                myouTubePlayer.cueVideo("EeU_dc8VcDQ");
                myouTubePlayer.loadVideo("EeU_dc8VcDQ");
                myouTubePlayer.setFullscreen(fullScreenVideo);
                Toast.makeText(YoutubeVideoPlayer.this, "Playing", Toast.LENGTH_SHORT).show();
                myouTubePlayer.play();
*/
/*

                if (!wasRestored) {

                    // loadVideo() will auto play video
                    // Use cueVideo() method, if you don't want to play it automatically
                    youTubePlayer.loadVideo("EeU_dc8VcDQ");

                    // Hiding player controls
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(YoutubeVideoPlayer.this, "Failed", Toast.LENGTH_SHORT).show();
            }

        };

        onFullscreenListener = new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {

            }
        };

        youTubePlayerView.initialize(YouTubeConfig.getApiKey(),onInitializedListener);
*/
        youTubePlayerView.initialize(YouTubeConfig.getApiKey(),this);

    }
    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_player_view);
    }
    private boolean loadFullScreenVideo(){
        SharedPreferences defaultSharedPreferences = getApplicationContext().getSharedPreferences(getApplicationContext().getPackageName(), Activity.MODE_PRIVATE);
        boolean fullscreen = defaultSharedPreferences.getBoolean("fullscreen", false);
//        Toast.makeText(this, "Fullscreen : "+fullscreen, Toast.LENGTH_SHORT).show();
        return fullscreen;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        System.out.println("SUCCESS : ");
        youTubePlayer.setFullscreen(loadFullScreenVideo());
        youTubePlayer.loadVideo("7OLBe5bt_fY");
//        myouTubePlayer = youTubePlayer;
        if (!wasRestored) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            youTubePlayer.loadVideo("7OLBe5bt_fY");

            // Hiding player controls
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        }
//        youTubePlayer.cueVideo("b8X_L_pXTTw");

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        System.out.println("ERROR : "+youTubeInitializationResult.toString());
//        Toast.makeText(this, "Failed Youtube", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFullscreen(boolean b) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
/*
            case R.id.favourite:
                if (news.isFav()){
                    DBHandler.favoriteNews(news, false);
                    favourite.setImageResource(R.drawable.ic_fav);
                    news.setFav(false);
                }
                else {
                    DBHandler.favoriteNews(news, true);
                    favourite.setImageResource(R.drawable.favorite_selected_48);
                    news.setFav(true);
                }
                break;
*/
/*            case R.id.share_news_detail:
                Utils.shareText(this, news.getTitle());
                break;*/

            case R.id.share:
                Utils.shareText(this, news.getTitle());
                break;
            case R.id.like:
                like.setImageResource(R.drawable.ic_like_selected);
                dislike.setImageResource(R.drawable.ic_dislike);
//                Toast.makeText(this, "Liked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dislike:

                dislike.setImageResource(R.drawable.ic_dislike_selected);
                like.setImageResource(R.drawable.ic_like);
//                Toast.makeText(this, "disliked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.favorite:
                boolean isFav = isFavorite(news);
                if (isFav){
                    DBHandler.favoriteNews(news, false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        favorite.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorPrimary));
                    }
                    this.favorite.setImageResource(R.drawable.ic_fav);
                    news.setFav(false);
                }
                else {
                    DBHandler.favoriteNews(news, true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        favorite.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorAccent));
                    }
                    this.favorite.setImageResource(R.drawable.favorite_selected_48);
                    news.setFav(true);
                }
                break;
            default:
                break;


        }
    }
    private boolean isFavorite(News news) {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<News> results = realm.where(News.class).equalTo("id",news.getId()).findAll();
        if (results.isEmpty()){
            return false;
        }
        else {
            return true;
        }
    }


}
