package com.ethiodaily.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.ethiodaily.R;
import com.ethiodaily.Util.Notification;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadTheme();
//        Toast.makeText(this, "Theme : "+getTheme().toString(), Toast.LENGTH_SHORT).show();
//        setTheme(R.style.AppTheme);
        setContentView(R.layout.settings_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            final Preference themesPreference = findPreference("themes");
            themesPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    int selectedTheme = R.style.RedTheme;
                    switch (newValue.toString()){
                        case "Light":
                            selectedTheme = R.style.AppTheme;
                            Toast.makeText(getContext(), "Light", Toast.LENGTH_SHORT).show();
                            break;
                        case "Night":
                            selectedTheme = R.style.DarkTheme;
                            Toast.makeText(getContext(), "Night", Toast.LENGTH_SHORT).show();
                            break;
                        case "Red":
                            selectedTheme = R.style.RedTheme;
                            Toast.makeText(getContext(), "Night", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            break;
                    }
/*
                    ThemeUtil themeUtil = new ThemeUtil();
                    themeUtil.changeTheme();
*/
                    SharedPreferences defaultSharedPreferences = getContext().getSharedPreferences(getContext().getApplicationContext().getPackageName(), Activity.MODE_PRIVATE);
                    SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                    edit.putInt("selectedTheme", selectedTheme);
                    edit.putString("selected","Dark");
                    edit.apply();
                    getActivity().recreate();

/*
                        SettingsActivity settingsActivity = new SettingsActivity();
                        settingsActivity.changeTheme(R.style.DarkTheme);
*/
//                        getActivity().recreate();
                    return true;

                }

            });

            final Preference fullscreenVideo = findPreference("fullscreen");

            fullscreenVideo.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                SharedPreferences defaultSharedPreferences = getContext().getSharedPreferences(getContext().getApplicationContext().getPackageName(), Activity.MODE_PRIVATE);
                SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Toast.makeText(getContext(), "New Value"+newValue.toString(), Toast.LENGTH_SHORT).show();
                    edit.putBoolean("fullscreen",Boolean.valueOf(newValue.toString()));
                    edit.apply();
                    return true;
                }
            });
            final Preference dataSaverPreference = findPreference("datasaver");

            dataSaverPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                SharedPreferences defaultSharedPreferences = getContext().getSharedPreferences(getContext().getApplicationContext().getPackageName(), Activity.MODE_PRIVATE);
                SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Toast.makeText(getContext(), "New Value"+newValue.toString(), Toast.LENGTH_SHORT).show();
                    edit.putBoolean("datasaver",Boolean.valueOf(newValue.toString()));
                    edit.apply();
                    return true;
                }
            });

            final Preference notificationPreference = findPreference("pushnotification");

            notificationPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                SharedPreferences defaultSharedPreferences = getContext().getSharedPreferences(getContext().getApplicationContext().getPackageName(), Activity.MODE_PRIVATE);
                SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
//                    Toast.makeText(getContext(), "New Value"+newValue.toString(), Toast.LENGTH_SHORT).show();
                    edit.putBoolean("pushnotification",Boolean.valueOf(newValue.toString()));
                    edit.apply();
                    //        show notification
                    Notification notification = new Notification(getContext());
                    notification.createNotificationChannel();
                    notification.createCustomNotification();
                    notification.setNotificationTabAction();
                    notification.showNotificaiton();


                    return true;
                }
            });
        }

        private static Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String stringValue = newValue.toString();
                if (preference instanceof ListPreference){
                    ListPreference themesPreference = (ListPreference) preference;
                    String value = themesPreference.getValue();
                    System.out.println("THEME : "+value);

                    if (themesPreference.getKey().equals("themes")){
/*
                        String value = themesPreference.getValue();
                        System.out.println("THEME : "+value);
*/
                    }

                }
                else if (preference instanceof SwitchPreference){
                }
                return true;

            }
        };

    }

/*
    @Override
    public void setTheme(@Nullable Resources.Theme theme) {
        super.setTheme(theme);
    }

    @Override
    public Resources.Theme getTheme() {

        Resources.Theme theme = super.getTheme();
        theme.applyStyle(R.style.AppTheme, true);
        return theme;
    }
*/

    private boolean loadFullScreenVideo(){
        SharedPreferences defaultSharedPreferences = getApplicationContext().getSharedPreferences(getApplicationContext().getPackageName(), Activity.MODE_PRIVATE);
        boolean fullscreen = defaultSharedPreferences.getBoolean("fullscreen", false);
        return fullscreen;
    }

    public void loadTheme(){
        SharedPreferences prefs = getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
        int theme = prefs.getInt("selectedTheme",R.style.DarkTheme);
        Toast.makeText(this, "Themes "+ prefs.getString("selected","xxxx"), Toast.LENGTH_SHORT).show();
        setTheme(theme);
    }
    private void changeTheme(int theme){
/*
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        edit.putInt("selectedTheme",theme);
        edit.apply();
*/
        Configuration config = new Configuration();
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Preferences", MODE_PRIVATE).edit();
        editor.putInt("selectedTheme", theme);
        editor.apply();
    }


}