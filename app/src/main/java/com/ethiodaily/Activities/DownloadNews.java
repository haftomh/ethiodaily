package com.ethiodaily.Activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ethiodaily.Api.GetDataService;
import com.ethiodaily.Api.News;
import com.ethiodaily.Api.RetrofitClientInstance;
import com.ethiodaily.DB.DBHandler;
import com.ethiodaily.Model.NewsResponse;
import com.ethiodaily.R;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DownloadNews extends AppCompatActivity {

    private TextView progressTV, totalDownloadTV, downloadingCategoryTV;
    private ProgressBar progressBar;
    private Button cancel,start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_download_news);

        initVars();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(DownloadNews.this, "Downloading", Toast.LENGTH_SHORT).show();
//                downloadNews();

                DownloadNewsTask downloadNewsTask = new DownloadNewsTask();
                downloadNewsTask.execute("10","sport","politics","business");
                progressBar.setMax(30);
                cancel.setEnabled(true);
            }
        });
    }


    private void initVars() {
        progressTV = (TextView) findViewById(R.id.progress_tv);
        totalDownloadTV = (TextView) findViewById(R.id.total_download_tv);
        downloadingCategoryTV = (TextView) findViewById(R.id.downloading_category);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        cancel = (Button) findViewById(R.id.cancel_btn);
        start = (Button) findViewById(R.id.start_btn);
	    progressTV.setText("0");
	    totalDownloadTV.setText("/30");
	    cancel.setEnabled(false);

    }
    private void setDownloadingCategoryText(String category) {
        downloadingCategoryTV.setText(category);
    }

    private class DownloadNewsTask extends AsyncTask<String, String, Long> {

        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();


        protected Long doInBackground(String... categories) {
            GetDataService service = retrofit.create(GetDataService.class);
            for (int page = 1; page < 7; page++) {
                Call<NewsResponse> call = service.getNews(page);
                try {
                    Response<NewsResponse> response = call.execute();
                    List<News> newsList = response.body().getNewsList();
                    for (int i=0; i<newsList.size();i++) {
                        News news = newsList.get(i);
                        publishProgress(String.valueOf((page-1)*5 + i+1),"hello "+i);
                        DBHandler.downloadNews(news);

                        System.out.println("Downloaded Successfully : "+news.getTitle());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

/*
            Call<NewsResponse> call = service.getNews(5);
            try {
                Response<NewsResponse> response = call.execute();
                List<News> newsList = response.body().getNewsList();
                for (News news : newsList) {
                    DBHandler.downloadNews(news);
                    System.out.println("Downloaded Successfully : "+news.getTitle());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

*/

            long totalSize = 30;

/*
            for (int i = 0; i < count; i++) {
//                setDownloadingCategoryText(categories[i]);
                NewsParser newsParser = new NewsParser(getApplicationContext(),categories[i]);
                ArrayList<News> newsArrayList = newsParser.parseAllNews();
                for (News news: newsArrayList) {
                    dbHandler.downloadNews(news);
                    publishProgress((int) ((i / (float) count) * 100));
                    System.out.println("Downloaded : "+news.getTitle());
                }
//                totalSize += PlaceholderFragment.Downloader.downloadFile(urls[i]);
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }
*/

            return totalSize;
        }


        protected void onProgressUpdate(String... progress) {
            System.out.println("OnProgressUpdate");
            progressBar.setProgress(Integer.valueOf(progress[0]));
            Toast.makeText(DownloadNews.this, ""+progress[0], Toast.LENGTH_SHORT).show();
            progressTV.setText(progress[0]+"");
            downloadingCategoryTV.setText(progress[1]);

        }

        protected void onPostExecute(Long result) {
//            Toast.makeText(DownloadNews.this, "Downloading Finished", Toast.LENGTH_LONG).show();
//            showDialog("Downloaded " + result + " bytes");
            Toast.makeText(DownloadNews.this, "Downloading Completed", Toast.LENGTH_SHORT).show();
        }
    }


}
