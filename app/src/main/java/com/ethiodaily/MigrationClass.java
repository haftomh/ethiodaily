package com.ethiodaily;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

public class MigrationClass implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0){
            schema.create("News")
                    .addField("title", String.class)
                    .addField("comment", String.class)
                    .addField("type", String.class)
                    .addField("categorie", String.class)
                    .addField("source", String.class)
                    .addField("date", String.class)
                    .addField("cover_image", String.class)
                    .addField("created_at", String.class)
                    .addField("updated_at",String.class)
                    .addField("id",int.class)
                    .addField("fav",boolean.class);
            schema.create("Favorites")
                    .addField("",int.class)
                    .addField("",String.class)
                    .addField("",String.class);
            schema.create("Downloads")
                    .addField("id",int.class)
                    .addField("title",String.class)
                    .addField("source",String.class)
                    .addField("url",String.class)
                    .addField("type",String.class)
                    .addField("category",String.class);
            schema.create("Favs")
                    .addField("id",int.class)
                    .addField("title",String.class)
                    .addField("source",String.class)
                    .addField("url",String.class)
                    .addField("type",String.class)
                    .addField("category",String.class);

            oldVersion++;

        }
    }
}
