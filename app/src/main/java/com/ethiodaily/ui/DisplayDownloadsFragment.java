package com.ethiodaily.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ethiodaily.Activities.DownloadNews;
import com.ethiodaily.Activities.NewsDetail;
import com.ethiodaily.Activities.YoutubeVideoPlayer;
import com.ethiodaily.Api.News;
import com.ethiodaily.Model.Downloads;
import com.ethiodaily.R;
import com.ethiodaily.ui.dummy.DummyContent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.io.Serializable;

import io.realm.Realm;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DisplayDownloadsFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private InterstitialAd mInterstitialAd;

//    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DisplayDownloadsFragment() {
    }



    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DisplayDownloadsFragment newInstance(int columnCount) {
        DisplayDownloadsFragment fragment = new DisplayDownloadsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
/*
        mAdView = rootView.findViewById(R.id.adView);
        final AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mAdView.loadAd(adRequest);
                    }
                });
            }

        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 1000 * 10);


        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
*/

        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_displaydownloads_list, container, false);
        TextView downloadCount = view.findViewById(R.id.downloads_count);
        Realm realm = Realm.getDefaultInstance();

        downloadCount.setText(String.valueOf(realm.where(Downloads.class).count()));

        Button download = view.findViewById(R.id.download_news_btn);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DownloadNews.class);
                getContext().startActivity(intent);
            }
        });
        // Set the adapter
//        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            DummyContent.addItems();
            final MyDisplayDownloadsRecyclerViewAdapter adapter = new MyDisplayDownloadsRecyclerViewAdapter(DummyContent.ITEMS, this.getContext());
            adapter.setListener(new MyDisplayDownloadsRecyclerViewAdapter.OnNewsClickListener() {
                @Override
                public void onNewsItemClick(Downloads news) {
                    Intent intent = new Intent(getContext(), YoutubeVideoPlayer.class);
                    News news1 = downloadsToNews(news);
                    intent.putExtra("NEWS", news1);
                    startActivity(intent);
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/W3wFpOPodsA?list=RDW3wFpOPodsA")));
                }

                @Override
                public void OnNewsClick(Downloads news) {
                    News news1 = downloadsToNews(news);
                    beginSecondActivity(news1);

                    // [START create_interstitial_ad_listener]
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            requestNewInterstitial();
                        }

                        @Override
                        public void onAdLoaded() {
                            // Ad received, ready to display
                            // [START_EXCLUDE]

                            // [END_EXCLUDE]
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
//                            Toast.makeText(getContext(), "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
                            // See https://goo.gl/sCZj0H for possible error codes.
//                            Log.w(TAG, "onAdFailedToLoad:" + i);
                        }
                    });
                    // [END create_interstitial_ad_listener]


                    if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
                    } else {

                        Intent intent = new Intent(getContext(), NewsDetail.class);
                        intent.putExtra("TITLE", news.getTitle());
                        intent.putExtra("NEWS", (Serializable) news1);
//                    intent.putExtra("NEWS",  news1);
                        startActivity(intent);
                    }


//                    Toast.makeText(getContext(), ""+position, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void OnNewsRemove(int position) {
                    removeNews(position);
                }

                private void removeNews(int position) {
                    DummyContent.ITEMS.remove(position);
                    adapter.notifyItemRemoved(position);
//                    Toast.makeText(0(), ""+position, Toast.LENGTH_SHORT).show();
                }

            });


            recyclerView.setAdapter(adapter);
//        }
        return view;
    }

    private News downloadsToNews(Downloads news) {
        News news1 = new News();
        news1.setId(Integer.valueOf(news.getId()));
        news1.setTitle(news.getTitle());
        news1.setComment(news.getUrl());
        news1.setCategorie(news.getCategory());
        news1.setType(news.getType());
        news1.setCover_image(news.getCover_image());
        news1.setCreated_at(news.getCreated_at());
        news1.setUpdated_at(news.getUpdated_at());
        news1.setSource(news.getSource());

        return news1;
    }

    private void beginSecondActivity(News news) {
        Intent intent = new Intent(getContext(), NewsDetail.class);
        intent.putExtra("TITLE", news.getTitle());
        intent.putExtra("NEWS",  news);

        startActivity(intent);
    }

    /**
     * Load a new interstitial ad asynchronously.
     */
    // [START request_new_interstitial]
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }
    // [END request_new_interstitial]

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Downloads item);
    }
}
