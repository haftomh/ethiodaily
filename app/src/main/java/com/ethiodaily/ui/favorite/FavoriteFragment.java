package com.ethiodaily.ui.favorite;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ethiodaily.Activities.NewsDetail;
import com.ethiodaily.Activities.YoutubeVideoPlayer;
import com.ethiodaily.Adapters.NewsAdapter;
import com.ethiodaily.Api.News;
import com.ethiodaily.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.OrderedRealmCollectionSnapshot;
import io.realm.Realm;
import io.realm.RealmResults;

public class FavoriteFragment extends Fragment {

    private FavoriteViewModel favoriteViewModel;
	private InterstitialAd mInterstitialAd;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        View root = inflater.inflate(R.layout.news_container, container, false);

        TextView news_log = root.findViewById(R.id.news_availability_log);

        RecyclerView newsList = (RecyclerView) root.findViewById(R.id.newsList);
        newsList.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),1);


        final Realm realm = Realm.getDefaultInstance();
        RealmResults<News> allNews = realm.where(News.class).equalTo("fav",true).findAll();

// Use an iterator to invite all allNews
        realm.beginTransaction();

        final ArrayList<News> newsArrayList = new ArrayList<>();
        for (News favs : allNews) {
            newsArrayList.add(favs);
            System.out.println("FAV DATABASE "+favs.toString());
        }
        realm.commitTransaction();

// Use a snapshot to invite all allNews
        realm.beginTransaction();
        OrderedRealmCollectionSnapshot<News> allNewsSnapshot = allNews.createSnapshot();
        for (int i = 0; i<allNewsSnapshot.size(); i++) {
        }
        realm.commitTransaction();
	    MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
		    @Override
		    public void onInitializationComplete(InitializationStatus initializationStatus) {
		    }
	    });
/*
        mAdView = rootView.findViewById(R.id.adView);
        final AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mAdView.loadAd(adRequest);
                    }
                });
            }

        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 1000 * 10);


        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
*/

	    mInterstitialAd = new InterstitialAd(getContext());
	    mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");


        if (allNews.size()>0){
            news_log.setVisibility(View.GONE);
            final NewsAdapter newsAdapter = new NewsAdapter(getContext(), newsArrayList, true);

            newsAdapter.setOnNewsClickListenter(new NewsAdapter.OnNewsClickListener() {
                @Override
                public void OnNewsClick(int position) {
                    System.out.println("Position : "+position);
                    Toast.makeText(getContext(), "Position : "+position, Toast.LENGTH_SHORT).show();
                    Toast.makeText(getContext(), "Position 2: "+newsArrayList.get(position-1), Toast.LENGTH_SHORT).show();
	                beginSecondActivity(newsArrayList.get(position));

	                // [START create_interstitial_ad_listener]
	                mInterstitialAd.setAdListener(new AdListener() {
		                @Override
		                public void onAdClosed() {
			                requestNewInterstitial();
		                }

		                @Override
		                public void onAdLoaded() {
			                // Ad received, ready to display
			                // [START_EXCLUDE]

			                // [END_EXCLUDE]
		                }

		                @Override
		                public void onAdFailedToLoad(int i) {
//                            Toast.makeText(getContext(), "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
			                // See https://goo.gl/sCZj0H for possible error codes.
//                            Log.w(TAG, "onAdFailedToLoad:" + i);
		                }
	                });
	                // [END create_interstitial_ad_listener]


	                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
	                } else {

		                Intent intent = new Intent(getContext(), NewsDetail.class);
		                intent.putExtra("TITLE", newsArrayList.get(position).getTitle());
		                intent.putExtra("NEWS", (Serializable) newsArrayList.get(position));
//                    intent.putExtra("NEWS",  news1);
//		                startActivity(intent);
	                }


                }

                @Override
                public void OnNewsFavorite(int position, boolean fav) {
                    deleteFavorites(position);
                    removeItem(position);
                }

                private void deleteFavorites(int position) {

                    // obtain the results of a query
                    final RealmResults<News> results = realm.where(News.class).equalTo("fav",true).and().equalTo("id",newsArrayList.get(position).getId()).findAll();

// All changes to data must happen in a transaction
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            // Delete all matches
                            results.deleteAllFromRealm();
                        }
                    });

                }

                private void removeItem(int position) {
                    newsArrayList.remove(position);
                    newsAdapter.notifyItemRemoved(position);
                }

                @Override
                public void OnNewsRemove(int position) {

                }

                @Override
                public void onNewsItemClick(int position) {
                    Intent intent = new Intent(getContext(), YoutubeVideoPlayer.class);

                    intent.putExtra("TITLE", newsArrayList.get(position).getTitle());
                    intent.putExtra("NEWS", newsArrayList.get(position));
                    startActivity(intent);

                }
            });

            newsList.setAdapter(newsAdapter);

        }
        else {
            news_log.setText("No favorites found! Please Press the heart symbol on the News title if you like.");
            news_log.setVisibility(View.VISIBLE);
        }

        newsList.setLayoutManager(layoutManager);
        return root;
    }
	private void beginSecondActivity(News news) {
		Intent intent = new Intent(getContext(), NewsDetail.class);
		intent.putExtra("TITLE", news.getTitle());
		intent.putExtra("NEWS",  news);
		startActivity(intent);
	}
	private void requestNewInterstitial() {
		AdRequest adRequest = new AdRequest.Builder()
			.build();

		mInterstitialAd.loadAd(adRequest);
	}

}