package com.ethiodaily.ui.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<com.ethiodaily.Model.Downloads> ITEMS = new ArrayList<com.ethiodaily.Model.Downloads>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, com.ethiodaily.Model.Downloads> ITEM_MAP = new HashMap<String, com.ethiodaily.Model.Downloads>();

    private static final int COUNT = 25;

    public static void addItems() {
        ITEMS.clear();
        ITEM_MAP.clear();
        // Add some sample items.
        Realm realm = Realm.getDefaultInstance();
        RealmResults<com.ethiodaily.Model.Downloads> downloadsRealmResults = realm.where(com.ethiodaily.Model.Downloads.class).findAll();

        for (com.ethiodaily.Model.Downloads downloads: downloadsRealmResults) {
            System.out.println("Downloads Item : "+ downloads.getTitle());
            addItem(downloads);
        }
    }

    private static void addItem(com.ethiodaily.Model.Downloads item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.getId(), item);
    }

    private static Downloads createDownloads(int position) {
        return new Downloads(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class Downloads {
        public final String id;
        public final String content;
        public final String details;

        public Downloads(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
