package com.ethiodaily.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ethiodaily.Api.GetDataService;
import com.ethiodaily.Api.RetrofitClientInstance;
import com.ethiodaily.Model.Category;
import com.ethiodaily.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {




    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.breaking, R.string.Politica, R.string.sport,R.string.world, R.string.Entertainment, R.string.business,R.string.social, R.string.psychology, R.string.health, R.string.tech, R.string.tourism,R.string.crime, R.string.fashion, R.string.other};
    private static final String[] TAB_TAGS = new String[]{"Breaking", "politica", "sport","world", "entertainment", "business","social","psychology","health", "tech", "tourism", "crime", "fashion", "other"};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;

        getCategories();
    }

    private void getCategories() {
        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Category>> call = service.getAllCategories();
        System.out.println("Response : "+call.toString());
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                generateDataList(response.body());
                System.out.println("Success :");
//                Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
//                Toast.makeText(mContext, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
//                Toast.makeText(mContext, "Error : "+t.toString(), Toast.LENGTH_SHORT).show();
                System.out.println("ERROR : "+t.toString());
            }
        });

    }

    private void generateDataList(List<Category> body) {
//        Toast.makeText(mContext, "Categories : "+body.size(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
//        Toast.makeText(mContext, "Tag "+TAB_TAGS[position], Toast.LENGTH_SHORT).show();
        PlaceholderFragment placeholderFragment = new PlaceholderFragment();
        return placeholderFragment.newInstance(position, TAB_TAGS[position]);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
//        Toast.makeText(mContext, "Title "+TAB_TITLES[position], Toast.LENGTH_SHORT).show();
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return TAB_TITLES.length;
    }
}