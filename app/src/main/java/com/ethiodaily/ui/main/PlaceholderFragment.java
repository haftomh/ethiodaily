package com.ethiodaily.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ethiodaily.Activities.NewsDetail;
import com.ethiodaily.Activities.YoutubeVideoPlayer;
import com.ethiodaily.Adapters.NewsAdapter;
import com.ethiodaily.Adapters.NewsPagedAdapter;
import com.ethiodaily.Api.DataParser;
import com.ethiodaily.Api.GetDataService;
import com.ethiodaily.Api.News;
import com.ethiodaily.Api.NewsParser;
import com.ethiodaily.Api.NewsViewModel;
import com.ethiodaily.Api.RetrofitClientInstance;
import com.ethiodaily.Model.NewsResponse;
import com.ethiodaily.PaginationScrollListener;
import com.ethiodaily.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.ethiodaily.PaginationScrollListener.PAGE_START;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private InterstitialAd mInterstitialAd;

    ArrayList<com.ethiodaily.Api.News> news = new ArrayList<>();


    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final String TAG = "tag";

    private PageViewModel pageViewModel;
    NewsAdapter.CustomViewHolder viewHolder;

    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;

    NewsAdapter newsAdapter = null;

    SwipeRefreshLayout swipeRefresh;
    static String category = "";
    private AdView mAdView;
    GetDataService newsService;
    Call<List<News>> newsCall;
    Call<NewsResponse> newsResponseCall;
    public  PlaceholderFragment newInstance(int index, String tag) {
        category = tag;
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putString(TAG,tag);
        fragment.setArguments(bundle);
/*

        System.out.println("Loading News");
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        newsService = (GetDataService) retrofit.create(GetDataService.class);
        newsCall = newsService.getAllPhotos();
        Response<List<com.ethiodaily.Api.News>> response = null;
        try {
            response = newsCall.execute();
            System.out.println("News From Localhost : ");
            List<com.ethiodaily.Api.News> list = response.body();
            for (com.ethiodaily.Api.News n: list){
                news.add(getNews(n));
                System.out.println("News : "+getNews(n).getTitle()+getNews(n).getImage_url());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 0;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
//            Toast.makeText(getContext(), index+" "+getArguments().getString(TAG), Toast.LENGTH_SHORT).show();
//
        }
        pageViewModel.setIndex(index);
        newsService = (GetDataService) RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_container, container, false);
//                        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//                        textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        swipeRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        final RecyclerView newsListRV = (RecyclerView) rootView.findViewById(R.id.newsList);
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(),1);


        DataParser dataParser = new DataParser(getContext());
//        final List<HashMap<String, String>> sources = dataParser.ParseData("sources",getArguments().getString(TAG));
        NewsParser newsParser = new NewsParser(getContext(), getArguments().getString(TAG));
//        news = newsParser.parseAllNewsFromWebServer();


        System.out.println("Loading News...");
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
//        GetDataService service = (GetDataService) retrofit.create(GetDataService.class);
        newsCall = newsService.getAllPhotos();
/*
            newsCall.enqueue(new Callback<List<com.ethiodaily.Api.News>>() {
                ArrayList<News> newsArrayList = new ArrayList<>();
                @Override
                public void onResponse(Call<List<com.ethiodaily.Api.News>> call, Response<List<com.ethiodaily.Api.News>> response) {
                    for (com.ethiodaily.Api.News news : response.body()) {
                        System.out.println(getNews(news).getTitle());
//                        newsArrayList.add(getNews(news));
                    }
//                    newsAdapter.addItems(newsArrayList);
                }

                @Override
                public void onFailure(Call<List<com.ethiodaily.Api.News>> call, Throwable t) {

                }
            });
*/
        newsResponseCall = newsService.getAllNews();
        newsResponseCall.enqueue(new Callback<NewsResponse>() {

            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
//                Toast.makeText(context, response.body().getNewsList().toString(), Toast.LENGTH_SHORT).show();;
                System.out.println("Succedd");
                System.out.println("NewsResponse : "+response.body().getFirstPageUrl());
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {

                System.out.println("NewsResponseFailure : "+t.toString());
            }
        });

/*        try {
//            newsCall.execute();
*/
/*
            Response<List<com.ethiodaily.Api.News>> listResponse = newsCall.execute();
            List<com.ethiodaily.Api.News> list = listResponse.body();
//            news.clear();
            for (com.ethiodaily.Api.News n: list){
                news.add(getNews(n));
            }
*//*
        } catch (Exception e) {
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }*/
//                news.clear();
/*
        try {
            Response<List<com.ethiodaily.Api.News>> NewsList = newsCall.execute();
            for (com.ethiodaily.Api.News News: NewsList.body()){
                news.add(getNews(News));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
*/
/*

        try {
            Response<List<com.ethiodaily.Api.News>> listResponse = newsCall.execute();
            System.out.println("News From Localhost : ");
            List<com.ethiodaily.Api.News> list = listResponse.body();
            for (com.ethiodaily.Api.News n: list){
                news.add(getNews(n));
                System.out.println("News : "+getNews(n).getTitle()+getNews(n).getImage_url());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
*/
/*
        newsCall.enqueue(new Callback<List<com.ethiodaily.Api.News>>() {
            @Override
            public void onResponse(Call<List<com.ethiodaily.Api.News>> call, Response<List<com.ethiodaily.Api.News>> response) {
                System.out.println("News From Localhost : ");
                List<com.ethiodaily.Api.News> list = response.body();
                for (com.ethiodaily.Api.News n: list){
                    news.add(getNews(n));
                    System.out.println("News : "+getNews(n).getTitle()+getNews(n).getImage_url());
                }
            }

            private News getNews(com.ethiodaily.Api.News n) {
                News news = new News();
                news.setTitle(n.getTitle());
                news.setType(n.getType());
                news.setUrl(n.getComment());
                news.setImage_url(""+n.getCover_image());
                news.setSource("fanabc");
                news.setCategory(n.getCategorie());
                return news;
            }

            @Override
            public void onFailure(Call<List<com.ethiodaily.Api.News>> call, Throwable t) {
                System.out.println("Error News : "+t.toString());
            }
        });
*/




//        Toast.makeText(getContext(), getArguments().getString(TAG), Toast.LENGTH_SHORT).show();
        newsListRV.setLayoutManager(layoutManager);
        newsListRV.setHasFixedSize(true);




        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
/*
        mAdView = rootView.findViewById(R.id.adView);
        final AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mAdView.loadAd(adRequest);
                    }
                });
            }

        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 1000 * 10);


        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
*/

        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

//        mInterstitialAd.loadAd(new AdRequest.Builder().build());



        final NewsAdapter.CustomViewHolder[] viewHolder = {(NewsAdapter.CustomViewHolder) newsListRV.findViewHolderForAdapterPosition(0)};
        newsListRV.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected int getTotalPageCount() {
                return 0;
            }

            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
//                doApiCall();


//                Toast.makeText(getContext(), category, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
//        if (news.size()>0){

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
//                doApiCall();
//                Toast.makeText(getContext(), category, Toast.LENGTH_SHORT).show();
                swipeRefresh.setRefreshing(false);
//                Toast.makeText(getContext(), "First :"+firstVisibleItemPosition, Toast.LENGTH_SHORT).show();
            }
        });


        final NewsPagedAdapter newsPagedAdapter = new NewsPagedAdapter();
        newsPagedAdapter.setContext(this.getContext());


        NewsViewModel newsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        newsViewModel.newsPagedList.observe(this, new Observer<PagedList<com.ethiodaily.Api.News>>() {
            @Override
            public void onChanged(PagedList<com.ethiodaily.Api.News> news) {
                newsPagedAdapter.submitList(news);
                newsPagedAdapter.notifyDataSetChanged();
//                LoadState loadState = new LoadState();
//                newsPagedAdapter.addLoadStateListener();
            }

        });

        newsAdapter = new NewsAdapter(getContext(), news);
        newsListRV.setAdapter(newsPagedAdapter);


        newsPagedAdapter.setOnNewsClickListenter(new NewsPagedAdapter.OnNewsClickListener() {
            @Override
            public void onNewsItemClick(News news) {
                Intent intent = new Intent(getContext(), YoutubeVideoPlayer.class);
                intent.putExtra("NEWS", news);
                startActivity(intent);
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/W3wFpOPodsA?list=RDW3wFpOPodsA")));
            }

            @Override
            public void OnNewsClick(News news) {
                beginSecondActivity(news);

                // [START create_interstitial_ad_listener]
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        requestNewInterstitial();
                    }

                    @Override
                    public void onAdLoaded() {
                        // Ad received, ready to display
                        // [START_EXCLUDE]

                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
//                            Toast.makeText(getContext(), "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
                        // See https://goo.gl/sCZj0H for possible error codes.
                        Log.w(TAG, "onAdFailedToLoad:" + i);
                    }
                });
                // [END create_interstitial_ad_listener]


                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
                } else {

                    Intent intent = new Intent(getContext(), NewsDetail.class);
                    intent.putExtra("TITLE", news.getTitle());
                    intent.putExtra("NEWS", (Serializable) news);
                    startActivity(intent);
                }


//                    Toast.makeText(getContext(), ""+position, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void OnNewsFavorite(int position, boolean fav) {
//                favoriteNews(position, fav);
            }

            @Override
            public void OnNewsRemove(int position) {
                removeNews(position);
            }

            private void removeNews(int position) {
                news.remove(position);
                newsAdapter.notifyItemRemoved(position);
//                    Toast.makeText(0(), ""+position, Toast.LENGTH_SHORT).show();
            }
/*
            private void favoriteNews(int position, final boolean fav) {
                Realm realm = Realm.getDefaultInstance();
                final News item = news.get(position);
                news.get(position).setFav(fav);
                if (!fav){
                    // obtain the results of a query
                    final RealmResults<News> results = realm.where(News.class).equalTo("fav",true).and().equalTo("id",item.getId()).findAll();

// All changes to data must happen in a transaction
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            // Delete all matches
                            results.deleteAllFromRealm();
                        }
                    });
                }else {
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            News favs = realm.createObject(News.class, item.getId()); // Create a new object
                            favs.setTitle(item.getTitle());
                            favs.setCategory(item.getCategory());
                            favs.setSource(item.getSource());
                            favs.setUrl(item.getUrl());
                            favs.setFav(fav);
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            System.out.println("DATA SAVED TO FAV");
                            // Transaction was a success.
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            // Transaction failed and was automatically canceled.
                            System.out.println("ERROR HANDLED ON FAV "+error.toString());
                        }
                    });

                }
//                    Toast.makeText(getContext(), "Fav : "+position, Toast.LENGTH_SHORT).show();
            }
*/


        });


//        }
        System.out.println("Implemented :");

        return rootView;

    }

    private void beginSecondActivity(News news) {
        Intent intent = new Intent(getContext(), NewsDetail.class);
        intent.putExtra("TITLE", news.getTitle());
        intent.putExtra("NEWS",  news);
//        News news1 = news;
//                    intent.putExtra("NEWS",  news1);
        startActivity(intent);
    }

    /**
     * Load a new interstitial ad asynchronously.
     */
    // [START request_new_interstitial]
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }
    // [END request_new_interstitial]


/*
    private void doApiCall() {
        final ArrayList<News> items = new ArrayList<>();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

*/
/*
                NewsParser newsParser = new NewsParser(getContext(), getArguments().getString(TAG));
                ArrayList<News> newsArrayList = newsParser.parseAllNews();

                for (News news :newsArrayList) {
//                    if (news.getCategory().equals(category)){
//                        items.add(news);
//                    }
                }
*//*

//                try {
                GetDataService NewsService = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<com.ethiodaily.Api.News>> NewsCall = NewsService.getAllPhotos();
                NewsCall.enqueue(new Callback<List<com.ethiodaily.Api.News>>() {
                    @Override
                    public void onResponse(Call<List<com.ethiodaily.Api.News>> call, Response<List<com.ethiodaily.Api.News>> response) {
                        for (com.ethiodaily.Api.News news : response.body()) {
                            System.out.println(getNews(news).getTitle());
                            items.add(getNews(news));
                        }
                        newsAdapter.addItems(items);
//                            newsAdapter.notify();
                    }

                    @Override
                    public void onFailure(Call<List<com.ethiodaily.Api.News>> call, Throwable t) {
                        System.out.println("Failed : ");
                    }
                });
//                    Response<List<com.ethiodaily.Api.News>> ListResponse = NewsCall.execute();


//                    } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
*/
/*
                newsCall.enqueue(new Callback<List<com.ethiodaily.Api.News>>() {
                    @Override
                    public void onResponse(Call<List<com.ethiodaily.Api.News>> call, Response<List<com.ethiodaily.Api.News>> response) {
                        System.out.println("News From Localhost : ");
                        List<com.ethiodaily.Api.News> list = response.body();
                        for (com.ethiodaily.Api.News n: list){
                            news.add(getNews(n));
                            System.out.println("News : "+getNews(n).getTitle()+getNews(n).getImage_url());
                        }
                    }

*//*

*/
/*
                for (int i = 0; i < 10; i++) {
                    itemCount++;
                    News news = new News();

                    news.setTitle("title " + itemCount);
                    items.add(news);
                }
*//*




                if (currentPage != PAGE_START)
                    newsAdapter.removeLoading();

//                newsAdapter.addItems(items);
                swipeRefresh.setRefreshing(false);

                // check weather is last page or not
                if (currentPage < totalPage) {
                    Toast.makeText(getContext(), "Loading", Toast.LENGTH_SHORT).show();
                    newsAdapter.addLoading();
                    newsAdapter.notifyDataSetChanged();
                } else {
                    isLastPage = true;
                }
                isLoading = false;
//        }
            }

        }, 1500);

    }
*/


    private void setProgressPercent(Integer progress) {
    }


    private static class Downloader {
        public static long downloadFile(URL url) {
            return 0;
        }
    }


    // [START add_lifecycle_methods]
    /** Called when leaving the activity */
    @Override
    public void onPause() {
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (!mInterstitialAd.isLoaded()) {
            requestNewInterstitial();
        }
    }
    private com.ethiodaily.Model.News getNews(com.ethiodaily.Api.News n) {
        com.ethiodaily.Model.News news = new com.ethiodaily.Model.News();
        news.setTitle(n.getTitle());
        news.setType(n.getType());
        news.setUrl(n.getComment());
        news.setImage_url(""+n.getCover_image());
        news.setSource("fanabc");
        news.setCategory(n.getCategorie());
        return news;
    }

}