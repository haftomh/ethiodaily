package com.ethiodaily.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ethiodaily.App.Config;
import com.ethiodaily.Model.Downloads;
import com.ethiodaily.R;
import com.ethiodaily.Util.Utils;

import java.util.List;


public class MyDisplayDownloadsRecyclerViewAdapter extends RecyclerView.Adapter<MyDisplayDownloadsRecyclerViewAdapter.ViewHolder> {

    private  Context context;
    private  List<com.ethiodaily.Model.Downloads> mValues;
    private OnNewsClickListener listener;
    public interface OnNewsClickListener{
        void OnNewsClick(Downloads news);
        void OnNewsRemove(int position);
        void onNewsItemClick(Downloads news);

    }

    public void setListener(OnNewsClickListener listener) {
        this.listener = listener;
    }
    //    private final OnListFragmentInteractionListener mListener;

    public MyDisplayDownloadsRecyclerViewAdapter(List<com.ethiodaily.Model.Downloads> items, Context context) {
        mValues = items;
        this.context = context;
//        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item_for_u, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        System.out.println("Binding : "+mValues.get(position).getTitle());
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getId());
        holder.newsTitleTV.setText(mValues.get(position).getTitle());
        holder.newsSourceTV.setText(mValues.get(position).getSource());
        holder.timeTV.setText(Utils.timeDifference(mValues.get(position).getCreated_at()));
        Glide.with(context).load(Config.baseUrl+
                "storage/cover_image/"+mValues.get(position).getCover_image())
                .fitCenter()
                .onlyRetrieveFromCache(getDatasaverStatus())
                .placeholder(R.drawable.ic_image_black_48dp)
                .into(holder.newsImage);
        if (mValues.get(position).getType().toLowerCase().equals("video")){
            holder.videoIndicator.setVisibility(View.VISIBLE);
//                videoIndicator.setImageResource(R.drawable.ic_video);
        }
        else {
            holder.videoIndicator.setVisibility(View.GONE);
        }



    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView newsTitleTV;
        public TextView newsSourceTV;
        public TextView timeTV;
        public ImageView videoIndicator, newsImage, removeNews;
        public com.ethiodaily.Model.Downloads mItem;
        OnNewsClickListener listener;
        public ViewHolder(View view, final OnNewsClickListener listener) {
            super(view);
            mView = view;
            this.listener = listener;
            mIdView = (TextView) view.findViewById(R.id.time);
            newsTitleTV = (TextView) view.findViewById(R.id.news_title);
            newsSourceTV = (TextView) view.findViewById(R.id.source_news_name);
            timeTV = (TextView) view.findViewById(R.id.time);
            newsImage = (ImageView) view.findViewById(R.id.news_image);
            videoIndicator = (ImageView) view.findViewById(R.id.video_indicator);

            removeNews = (ImageView) view.findViewById(R.id.remove_news);
            removeNews.setVisibility(View.VISIBLE);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        if (getAdapterPosition()!= RecyclerView.NO_POSITION){
                            if (mValues.get(getAdapterPosition()).getType().toLowerCase().equals("video")){
                                listener.onNewsItemClick(mValues.get(getAdapterPosition()));
                            }else{
                                listener.OnNewsClick(mValues.get(getAdapterPosition()));
                            }
                        }
                    }
                }
            });

            removeNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        if (getAdapterPosition()!= RecyclerView.NO_POSITION){
                            listener.OnNewsRemove(getAdapterPosition());
                        }
                    }
                }
            });

        }




        @Override
        public String toString() {
            return super.toString() + " '" + newsTitleTV.getText() + "'";
        }
    }
    private boolean getDatasaverStatus() {

        /*getting data saver mode status from sharedpreferences*/

        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        boolean datasaver = preferences.getBoolean("datasaver", false);
//        Toast.makeText(context, "Datasaver : "+datasaver, Toast.LENGTH_SHORT).show();

        return datasaver;
    }
}
