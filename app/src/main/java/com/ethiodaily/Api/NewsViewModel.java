package com.ethiodaily.Api;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

public class NewsViewModel extends ViewModel {
    public LiveData<PagedList<News>> newsPagedList;
    public LiveData<NewsDataSource> liveDataSource;

    public NewsViewModel() {
        init();
    }

    private void init() {
        NewsDataSourceFactory newsDataSourceFactory = new NewsDataSourceFactory();
        liveDataSource = newsDataSourceFactory.newsLiveDataSource;
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(NewsDataSource.PAGE_SIZE)
                .build();
        newsPagedList = new LivePagedListBuilder<>(newsDataSourceFactory, config).build();
    }
}
