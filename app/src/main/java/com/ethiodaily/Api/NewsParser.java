package com.ethiodaily.Api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewsParser {
    Context context;
    String category = null;

    public NewsParser(Context context) {
        this.context = context;
    }

    public NewsParser(Context context, String category) {
        this.context = context;
        this.category = category;
    }

    public ArrayList<News> parseAllNews(){
        String newsJson = DataParser.loadJSONFromAsset(context, "news");
        ArrayList<News> allNews = getAllNews(DataParser.getJSONArray(DataParser.getJsonObject(newsJson)));
        return allNews;
    }

    public ArrayList<News> parseAllNewsFromWebServer(){
        final ArrayList<News> newsList = new ArrayList<>();
        System.out.println("Loading News");
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
//        GetDataService service = (GetDataService) retrofit.create(GetDataService.class);
        GetDataService newsService = (GetDataService) retrofit.create(GetDataService.class);
        Call<List<com.ethiodaily.Api.News>> newsCall = newsService.getAllPhotos();
        try {
            System.out.println("News From Localhost : ");
            Response<List<com.ethiodaily.Api.News>> response = newsCall.execute();
            List<com.ethiodaily.Api.News> list = response.body();
            newsList.clear();
            for (com.ethiodaily.Api.News n: list){
                newsList.add(getNews(n));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
/*
        newsCall.enqueue(new Callback<List<com.ethiodaily.Api.News>>() {
            @Override
            public void onResponse(Call<List<com.ethiodaily.Api.News>> call, Response<List<com.ethiodaily.Api.News>> response) {
                System.out.println("News From Localhost : ");
                List<com.ethiodaily.Api.News> list = response.body();
                newsList.clear();
                for (com.ethiodaily.Api.News n: list){
                    newsList.add(getNews(n));
                }
            }

            private News getNews(com.ethiodaily.Api.News n) {
                News news = new News();
                news.setTitle(n.getTitle());
                news.setType(n.getType());
                news.setComment(n.getComment());
                news.setCover_image(""+n.getCover_image());
                news.setSource("fanabc");
                news.setCategorie(n.getCategorie());
                return news;
            }

            @Override
            public void onFailure(Call<List<com.ethiodaily.Api.News>> call, Throwable t) {
                System.out.println("Error News : "+t.toString());
            }
        });
*/

        System.out.println("News Size : "+ newsList.size());
        return newsList;
    }
    private News getNews(com.ethiodaily.Api.News n) {
        News news = new News();
        news.setTitle(n.getTitle());
        news.setType(n.getType());
        news.setComment(n.getComment());
        news.setCover_image(""+n.getCover_image());
        news.setSource("fanabc");
        news.setCategorie(n.getCategorie());
        return news;
    }
    public ArrayList<News> getAllNews(JSONArray jsonArray){
        final ArrayList<News> newsList = new ArrayList<>();
        News news = null;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                news = getNews(jsonArray.getJSONObject(i));
                if (news != null){
                    newsList.add(news);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Loading News");
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
//        GetDataService service = (GetDataService) retrofit.create(GetDataService.class);
        GetDataService newsService = (GetDataService) retrofit.create(GetDataService.class);
        Call<List<com.ethiodaily.Api.News>> newsCall = newsService.getAllPhotos();
        newsCall.enqueue(new Callback<List<com.ethiodaily.Api.News>>() {
            @Override
            public void onResponse(Call<List<com.ethiodaily.Api.News>> call, Response<List<com.ethiodaily.Api.News>> response) {
                System.out.println("News From Localhost : ");
                List<com.ethiodaily.Api.News> list = response.body();
                newsList.clear();
                for (com.ethiodaily.Api.News n: list){
                    newsList.add(getNews(n));
                }
            }

            private News getNews(com.ethiodaily.Api.News n) {
                News news = new News();
                news.setTitle(n.getTitle());
                news.setType(n.getType());
                news.setComment(n.getComment());
                news.setCover_image(""+n.getCover_image());
                news.setSource("fanabc");
                news.setCategorie(n.getCategorie());
                return news;
            }

            @Override
            public void onFailure(Call<List<com.ethiodaily.Api.News>> call, Throwable t) {
                System.out.println("Error News : "+t.toString());
            }
        });


        return newsList;
    }
    public News getNews(JSONObject JSONItem){
        final News news = new News();
        try {
            if (category==null || !category.equals(JSONItem.getString("category"))){
                return null;
            }
            news.setSource(JSONItem.getString("source"));
            news.setTitle(JSONItem.getString("title"));
            news.setCategorie(JSONItem.getString("category"));
            news.setComment(JSONItem.getString("url"));
            news.setCover_image(JSONItem.getString("image_url"));
            news.setType(JSONItem.getString("type"));
            news.setId(Integer.parseInt(JSONItem.getString("id")));

            Realm realm = Realm.getDefaultInstance();
            // obtain the results of a query
            final RealmResults<News> results = realm.where(News.class).equalTo("id",news.getId()).findAll();

// All changes to data must happen in a transaction
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    // Delete all matches
                    if (!results.isEmpty()){
                        news.setFav(results.get(0).isFav());
                    }
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return news;
    }

}
