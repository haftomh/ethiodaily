package com.ethiodaily.Api;

import com.ethiodaily.App.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
//    private static final String BASE_URL = "https://jsonplaceholder.typicode.com";
    private static final String BASE_URL = Config.baseUrl;

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {

            System.out.println("creating retrofit instance");
            Gson gson = new GsonBuilder()
                    .setLenient()

                    .create();
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
//                    .client()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}