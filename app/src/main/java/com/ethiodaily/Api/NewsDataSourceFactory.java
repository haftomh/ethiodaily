package com.ethiodaily.Api;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

public class NewsDataSourceFactory extends DataSource.Factory<Long, News>{

    public MutableLiveData<NewsDataSource> newsLiveDataSource = new MutableLiveData<>();

    @NonNull
    @Override
    public DataSource<Long, News> create() {
        NewsDataSource newsDataSource = new NewsDataSource();
        newsLiveDataSource.postValue(newsDataSource);
        return newsDataSource;
    }
}
