package com.ethiodaily.Api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DataParser {
    Context context;

    public DataParser(Context context) {
        this.context = context;
    }
    public List<HashMap<String, String>> ParseData(String what,String tag){
        String items = loadJSONFromAsset(context, what);
        ParseData(tag);
        HttpHandler handler = new HttpHandler();
//        String items = handler.makeServiceCall("https://raw.githubusercontent.com/haftomh/EthioVoice/master/news.json");
        System.out.println("Result "+items);
        JSONObject itemsJsonObject = getJsonObject(items);
        JSONArray itemsJsonArray = getJSONArray(itemsJsonObject);
        List<HashMap<String, String>> allItems = getAllItems(itemsJsonArray, what, tag);
        return allItems;
    }
    public List<HashMap<String, String>> ParseData(String what){
        System.out.println("Loading News");
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
//        GetDataService service = (GetDataService) retrofit.create(GetDataService.class);
        GetDataService newsService = (GetDataService) retrofit.create(GetDataService.class);
        Call<List<News>> news = newsService.getAllPhotos();
        news.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                generateDataList(response.body());
                System.out.println("News From Localhost : ");
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                System.out.println("Error News : "+t.toString());
            }
        });
/*

        Call<List<Category>> categories = service.getAllCategories();
        categories.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                System.out.println("News From Localhost "+response.body().toArray());
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                System.out.println("Error : "+t.toString());
            }
        });
*/
/*
        String items = loadJSONFromAsset(context, what);
        JSONObject itemsJsonObject = getJsonObject(items);
        JSONArray itemsJsonArray = getJSONArray(itemsJsonObject);
        List<HashMap<String, String>> allItems = getAllItems(itemsJsonArray, what, "all");
        return allItems;
*/
        return null;
    }
    public List<HashMap<String, String>> getAllItems(JSONArray jsonArray, String what, String tag){
        int counter = jsonArray.length();
        if (counter == 0){
            System.out.println("Counter == 0 : "+counter);
        }

        List<HashMap<String, String>> sourcesList = new ArrayList<>();
        HashMap<String, String> source = null;

        for (int i = 0; i<counter; i++){
            try {
                source = getSingleItem((JSONObject) jsonArray.get(i), what, tag);

                if (source!=null){
                    sourcesList.add(source);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return sourcesList;
    }

    public HashMap<String, String> getSingleItem(JSONObject JSONItem, String what, String tag){
        HashMap<String, String> item = new HashMap<>();
        try {
            String name = "";
            String id = "";
            String logo = "";
            String website = "";
            switch (what){
                case "sources":
                    name = JSONItem.getString("name");
                    id = JSONItem.getString("id");
                    logo = JSONItem.getString("logo");
                    website = JSONItem.getString("website");

                    item.put("NAME", name);
                    item.put("ID", id);
                    item.put("LOGO", logo);
                    item.put("WEBSITE", website);
                    break;
                case "news":


                    if (!tag.equals("all") && !JSONItem.getString("category").equals(tag)){
                        return null;
                    }
                    String title = JSONItem.getString("title");
                    String src = JSONItem.getString("source");
//                    id = JSONItem.getString("category");
                    item.put("TITLE", title);
                    System.out.println("NEWS : "+item.toString());
                    item.put("SOURCE", src);

                    break;


            }
        } catch (JSONException e) {
            System.out.println("EXCEPTION HANDLED");
            e.printStackTrace();
            return null;
        }

        return item;
    }



    private void generateDataList(List<News> photoList) {
        for (News photo: photoList) {
            System.out.println("Id : "+photo.getId());
            System.out.println("Title : "+photo.getTitle());
            System.out.println("Url : "+photo.getCover_image());
        }
    }

    public static String loadJSONFromAsset(Context context, String path) {
        String json = null;
        try {
            InputStream inputStream = context.getAssets().open(path+".json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static JSONObject getJsonObject(String json){

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static JSONArray getJSONArray(JSONObject jsonObject){
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

}
