package com.ethiodaily.Api;

import com.ethiodaily.Model.Category;
import com.ethiodaily.Model.NewsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("postdissplay")
    Call<List<News>> getAllPhotos();

    @GET("postdissplay")
    Call<NewsResponse> getNews(@Query("page") long page);
//    Call<NewsResponse> getNews(@Query("page") logn page);

    @GET("postdissplay")
    Call<NewsResponse> getAllNews();



    @GET("categoriedissplay")
    Call<List<Category>> getAllCategories();
}