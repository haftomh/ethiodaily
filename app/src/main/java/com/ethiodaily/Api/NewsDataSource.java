package com.ethiodaily.Api;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.ethiodaily.Model.NewsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewsDataSource extends PageKeyedDataSource<Long, News> {
    public static int PAGE_SIZE = 5;
    public static long FIRST_PAGE = 1;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull final LoadInitialCallback<Long, News> callback) {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        GetDataService dataService = retrofit.create(GetDataService.class);
        Call<NewsResponse> call = dataService.getNews(FIRST_PAGE);

        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                NewsResponse newsResponse = response.body();
                System.out.println("Path : "+newsResponse.getPath());
                if (newsResponse != null){
                    List<News> newsList = newsResponse.getNewsList();
                    System.out.println("News Size : "+newsList.size());
                    callback.onResult(newsList, null, FIRST_PAGE+1);
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                System.out.println("Error on fetching news");
            }
        });

    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Long> params, @NonNull final LoadCallback<Long, News> callback) {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        GetDataService dataService = retrofit.create(GetDataService.class);
        Call<NewsResponse> call = dataService.getNews(params.key);
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                NewsResponse newsResponse = response.body();
                if (newsResponse != null) {
                    List<News> newsList = newsResponse.getNewsList();
                    long key;
                    if(params.key > 1) {
                        key = params.key-1;
                    }
                    else {
                        key = 0;
                    }
                    callback.onResult(newsList, key);
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                System.out.println("Error on fetching news");

            }
        });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Long> params, @NonNull final LoadCallback<Long, News> callback) {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        GetDataService dataService = retrofit.create(GetDataService.class);
        Call<NewsResponse> call = dataService.getNews(params.key);
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                NewsResponse newsResponse = response.body();
                System.out.println("Path : "+newsResponse.getPath());
                if (newsResponse != null){
                    List<News> newsList = newsResponse.getNewsList();
                    System.out.println("News Size : "+newsList.size());
                    callback.onResult(newsList, params.key+1);
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                System.out.println("Error on fetching news");

            }
        });
    }
}
