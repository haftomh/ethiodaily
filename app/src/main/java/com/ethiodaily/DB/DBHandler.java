package com.ethiodaily.DB;

import com.ethiodaily.Api.News;
import com.ethiodaily.Model.Downloads;

import io.realm.Realm;
import io.realm.RealmResults;

public class DBHandler {


    private Realm realm;
    public DBHandler() {
        realm = Realm.getDefaultInstance(); // opens "myrealm.realm"
    }



    public static void favoriteNews(final News news, final boolean fav) {
        Realm realm = Realm.getDefaultInstance();
        news.setFav(fav);
        System.out.println("Fovorite : : ");
        if (!fav){
            // obtain the results of a query
            System.out.println("Removing from Favs");
            final RealmResults<News> results = realm.where(News.class).equalTo("fav",true).and().equalTo("id",news.getId()).findAll();

// All changes to data must happen in a transaction
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    // Delete all matches
                    results.deleteAllFromRealm();
                }
            });
        }else {
            System.out.println("Inserting to Favs");
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    News favs = realm.createObject(News.class, news.getId()); // Create a new object

                    favs.setTitle(news.getTitle());
                    favs.setCategorie(news.getCategorie());
                    favs.setSource(news.getSource());
                    favs.setComment(news.getComment());
                    favs.setType(news.getType());
                    favs.setFav(fav);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    System.out.println("DATA SAVED TO FAV");
                    // Transaction was a success.
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                    System.out.println("ERROR HANDLED ON FAV "+error.toString());
                }
            });

        }
//                    Toast.makeText(getContext(), "Fav : "+position, Toast.LENGTH_SHORT).show();
    }

    public static void downloadNews(final News newss){
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm rlm) {
	            Downloads downloadss = rlm.createObject(Downloads.class);
                System.out.println("Downloaded : "+newss.getTitle());
//	            downloadss= newss.toDownloads();
                downloadss.setId(String.valueOf(newss.getId()));
                downloadss.setCover_image(newss.getCover_image());
                downloadss.setCreated_at(newss.getCreated_at());
                downloadss.setUpdated_at(newss.getUpdated_at());
                downloadss.setUrl(newss.getComment());
                downloadss.setType(newss.getType());
                downloadss.setCategory(newss.getCategorie());
                downloadss.setSource(newss.getSource());
                downloadss.setTitle(newss.getTitle());
                rlm.commitTransaction();
            }

        });

    }

}
