package com.ethiodaily.App;

public class Config {


//    public static String baseUrl = "http://ethio-daily.com/";
public static String baseUrl = "http://192.168.1.7:8000/api/";
    public static String base = "http://192.168.1.7:8000/";
//public static String baseUrl = "http://192.168.137.141" +":8000/";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ethiovoice_firebase";
}
