package com.ethiodaily.Model;

import com.ethiodaily.Api.News;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsResponse {
    @SerializedName("data")
    private List<News> newsList;

    @SerializedName("current_page")
    private int currentPage;

    @SerializedName("from")
    private int from;

    @SerializedName("last_page")
    private int lastPage;

    @SerializedName("per_page")
    private int perPage;

    @SerializedName("to")
    private int to;

    @SerializedName("total")
    private int total;

    @SerializedName("first_page_url")
    private String firstPageUrl;

    @SerializedName("last_page_url")
    private String lastPageUrl;

    @SerializedName("next_page_url")
    private String nextPageUrl;

    @SerializedName("path")
    private String path;

    @SerializedName("prev_page_url")
    private String prevPageUrl;

//    getters

    public List<News> getNewsList() {
        return newsList;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getFrom() {
        return from;
    }

    public int getLastPage() {
        return lastPage;
    }

    public int getPerPage() {
        return perPage;
    }

    public int getTo() {
        return to;
    }

    public int getTotal() {
        return total;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public String getPrevPageUrl() {
        return prevPageUrl;
    }


//    Setters

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPrevPageUrl(String prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }
}
