package com.ethiodaily.Model;

import java.io.Serializable;

import io.realm.annotations.PrimaryKey;

public class News  implements Serializable {
    @PrimaryKey
    String id;
    String title;
    String source;
    String category;
    String url;
    String image_url;
    String type;
    boolean fav = false;

    public News() {
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public boolean isFav() {
        return fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "News{" +
                "id:'" + id + '\'' +
                ", title:'" + title + '\'' +
                ", source:'" + source + '\'' +
                ", url:'" + url + '\'' +
                ", type:'" + type + '\'' +
                ", category:'" + category + '\'' +
                '}';
    }

    public Downloads toDownloads(){
        Downloads downloads = new Downloads();
        downloads.setId(this.getId());
        downloads.setCategory(this.getCategory());
        downloads.setTitle(this.getTitle());
        downloads.setSource(this.getSource());
        downloads.setUrl(this.getUrl());

        return downloads;
    }
}
