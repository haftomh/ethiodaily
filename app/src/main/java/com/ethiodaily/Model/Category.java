package com.ethiodaily.Model;

import androidx.annotation.NonNull;

public class Category {

    private int id;
    private String categorie;

    private String name;
    private int order;
    private boolean selected;

    public Category(int id, String categorie) {
        this.id = id;
        this.categorie = categorie;
    }

    public Category(int id, String name, int order, boolean selected) {
        this.id = id;
        this.name = name;
        this.order = order;
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }
}
