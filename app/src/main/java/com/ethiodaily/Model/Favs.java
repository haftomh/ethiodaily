package com.ethiodaily.Model;

import io.realm.RealmObject;

public class Favs extends RealmObject {
    String id;
    String title;
    String source;
    String url;
    String type;
    String category;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }


    @Override
    public String toString() {
        return "News{" +
                "id:'" + id + '\'' +
                ", title:'" + title + '\'' +
                ", source:'" + source + '\'' +
                ", url:'" + url + '\'' +
                ", type:'" + type + '\'' +
                ", category:'" + category + '\'' +
                '}';
    }

}
