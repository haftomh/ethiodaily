package com.ethiodaily.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Downloads extends RealmObject {
    String id;
    String title;
    String source;
    String url;
    String type;
    String category;
    @SerializedName("cover_image")
    private String cover_image;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "News{" +
                "id:'" + id + '\'' +
                ", title:'" + title + '\'' +
                ", source:'" + source + '\'' +
                ", url:'" + url + '\'' +
                ", type:'" + type + '\'' +
                ", category:'" + category + '\'' +
                '}';
    }


}
